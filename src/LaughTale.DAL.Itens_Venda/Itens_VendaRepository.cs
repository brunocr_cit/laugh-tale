﻿using LaughTale.Modelos;
using System.Collections.Generic;

namespace LaughTale.DAL.Itens_Vendas
{
    public class Itens_VendaRepository: IItens_VendaRepository
    {
        private static List<Itens_VendaModel> Itens_Vendas_DB { get; set; }

        public Itens_VendaModel BuscarPorId(int id)
        {
            foreach (Itens_VendaModel itens_venda in Itens_Vendas_DB)
            {
                if (itens_venda.IdItens_Venda == id)
                {
                    return itens_venda;
                }
            }
            return null;
        }

        public IEnumerable<Itens_VendaModel> BuscarTodos()
        {
            return Itens_Vendas_DB;
        }

        public void Incluir(Itens_VendaModel obj)
        {
            Itens_Vendas_DB.Add(obj);
        }

        public void Excluir(Itens_VendaModel obj)
        {
            Itens_Vendas_DB.Remove(obj);
        }

        public void Alterar(Itens_VendaModel obj)
        {
            foreach (Itens_VendaModel itens_venda in Itens_Vendas_DB)
            {
                if (itens_venda.IdItens_Venda == obj.IdItens_Venda)
                {
                    Itens_Vendas_DB.Remove(itens_venda);
                    Itens_Vendas_DB.Add(obj);
                }
            }

        }
    }
}
