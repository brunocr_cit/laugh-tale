﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;


namespace LaughTale.DAL.Itens_Vendas
{
    public interface IVendaDao : IQueryRepository<Itens_VendaModel>, ICommandRepository<Itens_VendaModel>
    {
    }
}
