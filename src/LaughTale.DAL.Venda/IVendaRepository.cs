﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;


namespace LaughTale.DAL.Vendas
{
    public interface IVendaRepository : IQueryRepository<VendaModel>, ICommandRepository<VendaModel>
    {
    }
}
