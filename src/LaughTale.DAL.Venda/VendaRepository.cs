﻿using LaughTale.Modelos;
using System.Collections.Generic;


namespace LaughTale.DAL.Vendas
{
    public class VendaRepository : IVendaRepository
    {
        private static List<VendaModel> Vendas_DB { get; set; }

        public VendaModel BuscarPorId(int id)
        {
            foreach (VendaModel venda in Vendas_DB)
            {
                if(venda.IdVenda == id)
                {
                    return venda;
                }
            }
            return null;
        }

        public IEnumerable<VendaModel> BuscarTodos()
        {
            return Vendas_DB;
        }

        public void Incluir(VendaModel obj)
        {
            Vendas_DB.Add(obj);
        }

        public void Excluir(VendaModel obj)
        {
            Vendas_DB.Remove(obj);
        }

        public void Alterar(VendaModel obj)
        {
            foreach (VendaModel venda in Vendas_DB)
            {
                if (venda.IdVenda == obj.IdVenda)
                {
                    Vendas_DB.Remove(venda);
                    Vendas_DB.Add(obj);
                }
            }

        }
    }
}
