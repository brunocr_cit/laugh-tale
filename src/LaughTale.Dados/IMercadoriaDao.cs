﻿using LaughTale.Modelos.Mercadorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Dados
{
    public interface IMercadoriaDao : ICommand<Mercadoria>, IQuery<Mercadoria>
    {
    }
}
