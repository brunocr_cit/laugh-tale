﻿using LaughTale.Modelos.Operacoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Dados
{
    public interface IVendaDao : IQuery<Venda>, ICommand<Venda>
    {
    }
}
