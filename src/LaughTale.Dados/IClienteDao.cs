﻿using LaughTale.Modelos.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Dados
{
    public interface IClienteDao : IQuery<Cliente>, ICommand<Cliente>
    {
    }
}
