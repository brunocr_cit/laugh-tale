﻿using LaughTale.Modelos.Funcionarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Dados
{
    interface IFuncionarioDao : IQuery<Funcionario>, ICommand<Funcionario>
    {
    }
}
