﻿using LaughTale.Modelos.Fornecedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Dados
{
    public interface IFornecedorDao : IQuery<Fornecedor>, ICommand<Fornecedor>
    {
    }
}
