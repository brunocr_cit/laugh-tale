﻿using Microsoft.EntityFrameworkCore;
using Modelos.Clientes;
using Modelos.Fornecedores;
using Modelos.Funcionarios;
using Modelos.Mercadorias;
using Modelos.Operacoes;

namespace Modelos.Dados.ImplementacoesDao
{
    public class AppDBContext : DbContext
    {
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Fornecedor> Fornecedores { get; set; }
        public DbSet<Mercadoria> Mercadorias { get; set; }
        public DbSet<Venda> Vendas { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Server=127.0.0.1;Port=5432;Database=covil_das_madrinhas_dev;User Id=postgres;Password=123456;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Funcionario>()
                .HasKey(gerente => gerente.IdFuncionario)
                .HasName("PK_IdFuncionario");

             modelBuilder.Entity<Fornecedor>()
                .HasKey(fornecedor => fornecedor.IdFornecedor)
                .HasName("PK_IdFornecedor");

            modelBuilder.Entity<Mercadoria>()
                .HasKey(mercadoria => mercadoria.IdMercadoria)
                .HasName("PK_IdMercadoria");

            modelBuilder.Entity<Vestuario>()
                .HasOne(vestuario => vestuario.Fornecedor)
                .WithMany(fornecedor => fornecedor.)
                .HasForeignKey(vestuario => vestuario.Fornecedor.IdFornecedor);

           //modelBuilder.Entity<Mercadoria>()
           //    .HasOne(mercadoria => mercadoria.Estoquista)
           //    .WithMany(funcionario => funcionario.Mercadorias)
           //    .HasForeignKey(mercadoria => mercadoria.Estoquista.IdFuncionario);

            modelBuilder.Entity<Venda>()
                .HasKey(venda => venda.IdVenda)
                .HasName("PK_IdVenda");




        }
    }
}
