﻿using LaughTale.Modelos.Fornecedores;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Modelos.Dados.ImplementacoesDao
{
    public class FornecedorDao : IFornecedorDao
    {
        AppDBContext _context;

        public FornecedorDao(AppDBContext context)
        {
            _context = context;
        }
        public Fornecedor BuscarPorId(int id)
        {
            return _context.Fornecedores.Find(id);
        }

        public IEnumerable<Fornecedor> BuscarTodos()
        {
            return _context.Fornecedores.Include(l => l);
        }

        public void Excluir(Fornecedor obj)
        {
            _context.Fornecedores.Remove(obj);
            _context.SaveChanges();
        }

        public void Incluir(Fornecedor obj)
        {
            _context.Fornecedores.Add(obj);
            _context.SaveChanges();
        }

        public void Alterar(Fornecedor obj)
        {
            _context.Fornecedores.Update(obj);
            _context.SaveChanges();

        }
    }
}
