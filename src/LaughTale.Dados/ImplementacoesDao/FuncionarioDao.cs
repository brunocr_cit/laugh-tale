﻿using LaughTale.Modelos.Funcionarios;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Modelos.Dados.ImplementacoesDao
{
    public class FuncionarioDao : IFuncionarioDao
    {
        AppDBContext _context;

        public FuncionarioDao(AppDBContext context)
        {
            _context = context;
        }
        public Funcionario BuscarPorId(int id)
        {
            return _context.Funcionarios.Find(id);
        }

        public IEnumerable<Funcionario> BuscarTodos()
        {
            return _context.Funcionarios.Include(l => l);
        }

        public void Excluir(Funcionario obj)
        {
            _context.Funcionarios.Remove(obj);
            _context.SaveChanges();
        }

        public void Incluir(Funcionario obj)
        {
            _context.Funcionarios.Add(obj);
            _context.SaveChanges();
        }

        public void Alterar(Funcionario obj)
        {
            _context.Funcionarios.Update(obj);
            _context.SaveChanges();

        }
    }
}
