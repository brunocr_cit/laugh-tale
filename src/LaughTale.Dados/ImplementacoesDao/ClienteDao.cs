﻿using LaughTale.Modelos.Clientes;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Modelos.Dados.ImplementacoesDao
{
    public class ClienteDao : IClienteDao
    {
        AppDBContext _context;

        public ClienteDao(AppDBContext context)
        {
            _context = context;
        }
        public Cliente BuscarPorId(int id)
        {
            return _context.Clientes.Find(id);
        }

        public IEnumerable<Cliente> BuscarTodos()
        {
            return _context.Clientes.Include(l => l);
        }

        public void Excluir(Cliente obj)
        {
            _context.Clientes.Remove(obj);
            _context.SaveChanges();
        }

        public void Incluir(Cliente obj)
        {
            _context.Clientes.Add(obj);
            _context.SaveChanges();
        }

        public void Alterar(Cliente obj)
        {
            _context.Clientes.Update(obj);
            _context.SaveChanges();

        }
    }
}
