﻿using LaughTale.Modelos.Operacoes;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Modelos.Dados.ImplementacoesDao
{
    public class VendaDao : IVendaDao
    {
        AppDBContext _context;

        public VendaDao(AppDBContext context)
        {
            _context = context;
        }
        public Venda BuscarPorId(int id)
        {
            return _context.Vendas.Find(id);
        }

        public IEnumerable<Venda> BuscarTodos()
        {
            return _context.Vendas.Include(l => l);
        }

        public void Excluir(Venda obj)
        {
            _context.Vendas.Remove(obj);
            _context.SaveChanges();
        }

        public void Incluir(Venda obj)
        {
            _context.Vendas.Add(obj);
            _context.SaveChanges();
        }

        public void Alterar(Venda obj)
        {
            _context.Vendas.Update(obj);
            _context.SaveChanges();

        }
    }
}
