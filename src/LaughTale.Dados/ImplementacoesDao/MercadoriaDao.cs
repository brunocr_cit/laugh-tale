﻿using LaughTale.Modelos.Mercadorias;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Modelos.Dados.ImplementacoesDao
{
    public class MercadoriaDao : IMercadoriaDao
    {
        AppDBContext _context;

        public MercadoriaDao(AppDBContext context)
        {
            _context = context;
        }
        public Mercadoria BuscarPorId(int id)
        {
            return _context.Mercadorias.Find(id);
        }

        public IEnumerable<Mercadoria> BuscarTodos()
        {
            return _context.Mercadorias.Include(l => l);
        }

        public void Excluir(Mercadoria obj)
        {
            _context.Mercadorias.Remove(obj);
            _context.SaveChanges();
        }

        public void Incluir(Mercadoria obj)
        {
            _context.Mercadorias.Add(obj);
            _context.SaveChanges();
        }

        public void Alterar(Mercadoria obj)
        {
            _context.Mercadorias.Update(obj);
            _context.SaveChanges();

        }
    }
}
