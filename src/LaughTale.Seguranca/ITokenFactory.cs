﻿namespace LaughTale.Seguranca
{
    public interface ITokenFactory
    {
        string Token { get; }
    }
}
