﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;

namespace LaughTale.DAL.Funcionarios
{
    interface IFuncionarioRepository : IQuery<FuncionarioModel>, ICommand<FuncionarioModel>
    {
        FuncionarioModel BuscarPorNome(string nome);
    }
}
