﻿using LaughTale.Extensions;
using LaughTale.Modelos;
using System.Collections.Generic;


namespace LaughTale.DAL.Funcionarios
{
    public class FuncionarioRepository : IFuncionarioRepository
    {
        private static List<FuncionarioModel> Funcionarios_DB { get; set; }

        public FuncionarioRepository()
        {
            Funcionarios_DB.AdicionarVarios(
                new FuncionarioModel(new PessoaModel("Boa Hancock", "Mãe Boa", "123456789-01", "(01) 9 9876-5432", new EnderecoModel("Rua Boa", "001A", "Casa", "Bairro Boa", "12345-001", "Cidade Boa", "Estado Boa")), "Gerente", "123.45678.90-1", 5000, "Ger_Boa"),
                new FuncionarioModel(new PessoaModel("Shirahoshi", "Mãe Shirahoshi", "123456789-02", "(02) 9 9876-5432", new EnderecoModel("Rua Shirahoshi", "002A", "Apartamento 101", "Bairro Shirahoshi", "12345-002", "Cidade Shirahoshi", "Estado Shirahoshi")), "Gerente", "123.45678.90-2", 5000, "Ger_Shirahoshi"),
                new FuncionarioModel(new PessoaModel("Nico Robin", "Mãe Nico", "123456789-03", "(03) 9 9876-5432", new EnderecoModel("Rua Nico", "003A", "Casa", "Bairro Nico", "12345-003", "Cidade Nico", "Estado Nico")), "Vendedor", "123.45678.90-3", 3000, "Ven_Nico"),
                new FuncionarioModel(new PessoaModel("Nefertari Vivi", "Mãe Nefertari ", "123456789-04", "(04) 9 9876-5432", new EnderecoModel("Rua Nefertari ", "004A", "Apartamento 201", "Bairro Nefertari ", "12345-004", "Cidade Nefertari ", "Estado Nefertari ")), "Vendedor", "123.45678.90-4", 3000, "Ven_Nefertari "),
                new FuncionarioModel(new PessoaModel("Nami", "Mãe Nami", "123456789-05", "(05) 9 9876-5432", new EnderecoModel("Rua Nami", "005A", "Casa", "Bairro Nami", "12345-005", "Cidade Nami", "Estado Nami")), "Vendedor", "123.45678.90-5", 3000, "Ven_Nami"),
                new FuncionarioModel(new PessoaModel("Tama", "Mãe Tama", "123456789-11", "(11) 9 9876-5432", new EnderecoModel("Rua Tama", "0011A", "Apartamento 304", "Bairro Tama", "12345-011", "Cidade Tama", "Estado Tama")), "Estoquista", "123.45678.91-1", 2500, "Est_Tama")
                );
        }

        public FuncionarioModel BuscarPorId(int id)
        {
            foreach (FuncionarioModel funcionario in Funcionarios_DB)
            {
                if (funcionario.IdFuncionario == id)
                {
                    return funcionario;
                }
            }
            return null;
        }

        public FuncionarioModel BuscarPorNome(string nome)
        {
            foreach (FuncionarioModel funcionario in Funcionarios_DB)
            {
                if (funcionario.Pessoa.NomeCompleto == nome)
                {
                    return funcionario;
                }
            }
            return null;
        }

        public IEnumerable<FuncionarioModel> BuscarTodos()
        {
            return Funcionarios_DB;
        }

        public void Incluir(FuncionarioModel obj)
        {
            Funcionarios_DB.Add(obj);
        }

        public void Excluir(FuncionarioModel obj)
        {
            Funcionarios_DB.Remove(obj);
        }

        public void Alterar(FuncionarioModel obj)
        {
            foreach (FuncionarioModel funcionario in Funcionarios_DB)
            {
                if (funcionario.IdFuncionario == obj.IdFuncionario)
                {
                    Funcionarios_DB.Remove(funcionario);
                    Funcionarios_DB.Add(obj);
                }
            }

        }
    }
}
