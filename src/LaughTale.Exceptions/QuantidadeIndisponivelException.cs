﻿using System;

namespace LaughTale.Exceptions
{
    public class QuantidadeIndisponivelException : Exception
    {
        public int QuantidadeRequerida { get; }
        public int QuantidadeExistente { get; }

        public QuantidadeIndisponivelException()
        {

        }

        public QuantidadeIndisponivelException(int quantidadeRequerida, int quantidadeExistente)
            : this($"Tentativa de venda de {quantidadeRequerida} unidades, porém temos apenas {quantidadeExistente} no estoque")
        {
            QuantidadeRequerida = quantidadeRequerida;
            QuantidadeExistente = quantidadeExistente;
        }

        public QuantidadeIndisponivelException(string mensagem)
            : base(mensagem)
        {
        }

        public QuantidadeIndisponivelException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
