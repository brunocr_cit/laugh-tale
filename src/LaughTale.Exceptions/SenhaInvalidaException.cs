﻿using System;

namespace LaughTale.Exceptions
{
    public class SenhaInvalidaException : Exception
    {
        public SenhaInvalidaException()
        {

        }
        public SenhaInvalidaException(string mensagem)
            : base(mensagem)
        {
        }

        public SenhaInvalidaException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
