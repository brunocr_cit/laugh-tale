﻿using System;

namespace LaughTale.Exceptions
{
    public class PermissaoNegadaException : Exception
    {
        public PermissaoNegadaException()
        {

        }
        public PermissaoNegadaException(string mensagem)
            : base(mensagem)
        {
        }

        public PermissaoNegadaException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
