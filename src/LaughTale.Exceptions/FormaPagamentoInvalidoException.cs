﻿using System;

namespace LaughTale.Exceptions
{
    public class FormaPagamentoInvalidoException : Exception
    {
        public FormaPagamentoInvalidoException()
        {

        }
        public FormaPagamentoInvalidoException(string mensagem)
            : base(mensagem)
        {
        }

        public FormaPagamentoInvalidoException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
