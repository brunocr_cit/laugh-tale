﻿using System.Collections.Generic;

namespace LaughTale.Modelos
{
    public class FornecedorModel
    {
        public static int ContadorFornecedor { get; private set; }
        public int IdFornecedor { get; set; }
        public string NomeEmpresa { get; set; }
        public string CNPJ { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public EnderecoModel Endereco { get; set; }
        public List<MercadoriaModel> Mercadorias { get; set; }

        public FornecedorModel(string nomeEmpresa,
            string cnpj,
            string emailContato,
            string telefoneContato,
            EnderecoModel endereco)
        {
            NomeEmpresa = nomeEmpresa;
            CNPJ = cnpj;
            EmailContato = emailContato;
            TelefoneContato = telefoneContato;
            Endereco = endereco;
            ContadorFornecedor++;
            IdFornecedor = ContadorFornecedor;
        }

    }
}
