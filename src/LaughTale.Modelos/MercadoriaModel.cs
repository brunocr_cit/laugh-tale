﻿using LaughTale.Exceptions;

namespace LaughTale.Modelos
{
    public class MercadoriaModel
    {
        private int EstoqueTotal { get; set; }
        public int IdMercadoria { get;}
        public string Descricao { get; }
        public int Quantidade { get; set; }
        public double PrecoUnidadeFornecedor { get; }
        public double PrecoUnidadeCliente { get; set; }
        public FornecedorModel Fornecedor { get;}
        public FuncionarioModel Estoquista { get; }

        public MercadoriaModel(int idMercadoria, string descricao, int quantidade, double precoUnidadeFornecedor, double precoUnidadeCliente, FornecedorModel fornecedor, FuncionarioModel estoquista)
        {
            if (estoquista.Cargo != ECargo.Estoquista)
            {
                throw new PermissaoNegadaException($"O funcionario não tem permissão para cadastrar mercadorias.");
            }

            IdMercadoria = idMercadoria;
            Descricao = descricao;
            Quantidade = quantidade;
            PrecoUnidadeFornecedor = precoUnidadeFornecedor;
            PrecoUnidadeCliente = precoUnidadeCliente;
            Fornecedor = fornecedor;
            EstoqueTotal += quantidade;
            Estoquista = estoquista;
        }

        public void ModificarPreco(double valorModificado)
        {
            PrecoUnidadeCliente = valorModificado;
        }


    }
}
