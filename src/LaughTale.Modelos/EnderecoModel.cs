﻿namespace LaughTale.Modelos
{
    public class EnderecoModel
    {
        public string Logradouro { get; private set; }
        public string Numero { get; private set; }
        public string Complemento { get; set; }
        public string Bairro { get; private set; }
        public string CEP { get; set; }
        public string Cidade { get; private set; }
        public string Estado { get; private set; }

        public EnderecoModel(string logradouro, string numero, string complemento, string bairro, string cep, string cidade, string estado)
        {
            Logradouro = logradouro;
            Numero = numero;
            Complemento = complemento;
            Bairro = bairro;
            CEP = cep;
            Cidade = cidade;
            Estado = estado;
        }

    }
}
