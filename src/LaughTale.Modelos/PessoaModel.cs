﻿namespace LaughTale.Modelos
{
    public class PessoaModel
    {
        public string NomeCompleto { get; private set; }
        public string NomeDaMae { get; private set; }
        public string CPF { get; private set; }
        public string TelefoneContato { get; set; }
        public EnderecoModel Endereco { get; set; }

        public PessoaModel(string nomeCompleto,
            string nomeDaMae,
            string cpf,
            string telefoneContato,
            EnderecoModel endereco)
        {
            Endereco = endereco;
            NomeCompleto = nomeCompleto;
            NomeDaMae = nomeDaMae;
            CPF = cpf;
            TelefoneContato = telefoneContato;
        }
    }
}
