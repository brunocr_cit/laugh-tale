﻿namespace LaughTale.Modelos
{
    public class Itens_VendaModel
    {
        public static int QuantidadeItens_Vendas { get; private set; }
        public int IdItens_Venda { get; set; }
        public MercadoriaModel Mercadoria { get; set; }
        public int QuantidadeMercadoria { get; set; }
        public Itens_VendaModel(MercadoriaModel mercadoria, int quantidadeMercadoria)
        {
            Mercadoria = mercadoria;
            QuantidadeMercadoria = quantidadeMercadoria;
            QuantidadeItens_Vendas++;
            IdItens_Venda = QuantidadeItens_Vendas;
        }
    }
}
