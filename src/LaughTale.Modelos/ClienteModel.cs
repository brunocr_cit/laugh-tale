﻿namespace LaughTale.Modelos
{
    public class ClienteModel
    {
        public static int ContadorCliente { get; private set; }
        public int IdCliente { get; set; }
        public PessoaModel Pessoa { get; }
        public ClienteModel(PessoaModel pessoa)
        {
            Pessoa = pessoa;
            ContadorCliente++;
            IdCliente = ContadorCliente;
        }

    }
}
