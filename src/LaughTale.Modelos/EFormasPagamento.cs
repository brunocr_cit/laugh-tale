﻿namespace LaughTale.Modelos
{
    public enum EFormasPagamento
    {
        Dinheiro,
        CartaoCredito,
        CartaoDebito,
        Crediario
    }
}
