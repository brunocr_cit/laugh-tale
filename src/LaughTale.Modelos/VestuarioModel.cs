﻿namespace LaughTale.Modelos
{
    public class VestuarioModel : MercadoriaModel
    {
        public string Tamanho { get; }
        public VestuarioModel(int codigoBarras, 
            string nome, 
            int quantidade,
            double precoUnidadeFornecedor,
            double precoUnidadeCliente,
            FornecedorModel fornecedor, 
            FuncionarioModel estoquista,
            string tamanho) : 
            base(codigoBarras, 
                nome, 
                quantidade,
                precoUnidadeFornecedor,
                precoUnidadeCliente,
                fornecedor,
                estoquista)
        {
            Tamanho = tamanho;
        }

    }
}
