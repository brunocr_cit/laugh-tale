﻿using System;
using LaughTale.Exceptions;


namespace LaughTale.Modelos
{
    public class PagamentoModel
    {
        public EFormasPagamento FormaPagamento { get; } 
        public int QuantidadeParcelas { get; }
        public double ValorParcelas { get; }

        public PagamentoModel(string formaPagamento, int quantidadeParcelas, double valorVenda, ClienteModel cliente)
        {
            switch (formaPagamento)
            {
                case "Dinheiro": 
                    FormaPagamento = EFormasPagamento.Dinheiro;
                    break;
                case "CartaoCredito":
                    FormaPagamento = EFormasPagamento.CartaoCredito;
                    break;
                case "CartaoDebito":
                    FormaPagamento = EFormasPagamento.CartaoDebito;
                    break;
                case "Crediario":
                    FormaPagamento = EFormasPagamento.Crediario;
                    break;

                default: throw new ArgumentException($"A forma de pagamento, {formaPagamento}, enviada para cadastro é inválido.");
            }
            if (cliente == null && FormaPagamento == EFormasPagamento.Crediario)
            {
                throw new FormaPagamentoInvalidoException("Um cliente não cadastrado não pode realizar o pagamento por meio de parcelas no crediário.");
            }

            if (quantidadeParcelas < 1 || quantidadeParcelas > 10)
            {
                throw new ArgumentException("A quantidade de parcelas não pode ser menor que 1 e nem maior que 10.");
            }

            QuantidadeParcelas = quantidadeParcelas;
            ValorParcelas = valorVenda / quantidadeParcelas;

            CaixaRegistradoraModel.EntradaDinheiro(ValorParcelas); 

        }
       
    }
}
