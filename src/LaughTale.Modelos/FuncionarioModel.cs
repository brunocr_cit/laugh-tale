﻿using System;

namespace LaughTale.Modelos
{
    public class FuncionarioModel
    {
       public static int ContadorFuncionario { get; private set; }
        public int IdFuncionario { get;}
        public ECargo Cargo { get; set; }
        public string PIS { get;}
        public double Salario { get; private set; } 
        public string Senha { get; set; }
        public PessoaModel Pessoa { get; }
        public FuncionarioModel(PessoaModel pessoa,string cargo, string pis, double salario, string senha)
        {
            ContadorFuncionario++;
            IdFuncionario = ContadorFuncionario;
            switch (cargo)
            {
                case "Gerente":
                    Cargo = ECargo.Gerente;
                    break;
                case "Vendedor": Cargo = ECargo.Vendedor;
                    break;
                case "Estoquista":
                    Cargo = ECargo.Estoquista;
                    break;
                default: throw new ArgumentException($"O cargo, {cargo}, enviado para cadastro é inválido.");
                    
            }
            PIS = pis;
            Salario = salario;
            Senha = senha;
            Pessoa = pessoa;

        }
        public void ModificarSalario(double novoSalarioBase)
        {
            if (novoSalarioBase < 0)
            {
                throw new ArgumentException($"O novo salário não pode ser menor que zero, como o apresentado {novoSalarioBase}");
            }

            Salario = novoSalarioBase;
            
        }
        public double AcrescentarComissaoVendaNoSalario(double valorVenda)
        {
           
            if (valorVenda < 0)
            {
                throw new ArgumentException("O valor da venda não pode ser negativo.");
            }
            double comissaoVenda = 0.1 * valorVenda;
            Salario += comissaoVenda;

            return comissaoVenda;
        }
    }
}
