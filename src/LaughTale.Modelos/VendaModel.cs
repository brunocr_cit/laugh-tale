﻿using System;
using System.Collections.Generic;
using LaughTale.Exceptions;

namespace LaughTale.Modelos
{
    public class VendaModel 
    {
        public static int QuantidadeVendas { get; private set; }
        public int IdVenda { get; }
        public List<Itens_VendaModel> CarroCompras { get; }
        public FuncionarioModel Vendedor { get; }
        public ClienteModel Cliente { get; }
        public PagamentoModel Pagar { get; set; }
        public double ValorVenda { get; }
        public double ComissaoVenda { get; private set; }

        public VendaModel(List<Itens_VendaModel> carroCompras,
            FuncionarioModel vendedor,
            ClienteModel cliente,
            string formaPagamento,
            int quantidadeParcelas)
        {
            CarroCompras = carroCompras;
            Vendedor = vendedor;
            Cliente = cliente;

            foreach (Itens_VendaModel carrinho in CarroCompras)
            {
                if (carrinho.QuantidadeMercadoria > carrinho.Mercadoria.Quantidade)
                {
                    throw new QuantidadeExcedenteException($"Tentativa de comprar {carrinho.QuantidadeMercadoria} unidades da mercadoria {carrinho.Mercadoria.Descricao}, porém só temos {carrinho.Mercadoria.Quantidade} unidades no estoque.");
                }

                ValorVenda += carrinho.QuantidadeMercadoria * carrinho.Mercadoria.PrecoUnidadeCliente;
                carrinho.Mercadoria.Quantidade -= carrinho.QuantidadeMercadoria;
            }

            ComissaoVenda = vendedor.AcrescentarComissaoVendaNoSalario(ValorVenda);

            Pagar = new PagamentoModel(formaPagamento, quantidadeParcelas, ValorVenda, cliente);
            QuantidadeVendas++;
            IdVenda = QuantidadeVendas;
        }


        public void DetalhesVenda()
        {
            Console.WriteLine($"O cliente {Cliente.Pessoa.NomeCompleto} está comprando: ");

            foreach (var produtos in CarroCompras)
            {
                Console.WriteLine($"{produtos.QuantidadeMercadoria} peças do produto {produtos.Mercadoria.Descricao}");
                Console.WriteLine($"Restam no estoque {produtos.Mercadoria.Quantidade} unidades.");
            }

            Console.WriteLine($"O vendedor {Vendedor.Pessoa.NomeCompleto} adquiriu uma comissão de R${ComissaoVenda}," +
                $" totalizando um salário de R${Vendedor.Salario}");
            Console.WriteLine($"Dinheiro em caixa R$ {CaixaRegistradoraModel.QuantidadeDinheiro}");
            Console.WriteLine($"Até o momento já foram executadas {QuantidadeVendas} vendas");

        }
    }
}
