﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;

namespace LaughTale.DAL.Fornecedores
{
    public interface IFornecedorRepository : IQuery<FornecedorModel>, ICommand<FornecedorModel>
    {
        FornecedorModel BuscarPorNome(string nome);
    }
}
