﻿using LaughTale.Extensions;
using LaughTale.Modelos;
using System.Collections.Generic;

namespace LaughTale.DAL.Fornecedores
{
    public class FornecedorRepository : IFornecedorRepository
    {
        private static List<FornecedorModel> Fornecedores_DB { get; set; }

        public FornecedorRepository()
        {
            Fornecedores_DB.AdicionarVarios(
                new FornecedorModel("Empresa Kureha", "12.345.678/1234-01", "empresakureha@contato.com", "(09) 9 9876-5432", new EnderecoModel("Rua Kureha", "009A", "Galpão 44B", "Bairro Kureha", "12345-009", "Cidade Kureha", "Estado Kureha")),
                new FornecedorModel("Empresa Vinsmoke Reiju", "12.345.678/1234-02", "empresavinsmoke_reiju@contato.com", "(10) 9 9876-5432", new EnderecoModel("Rua Vinsmoke Reiju", "010A", "Galpão 23A", "Bairro Vinsmoke Reiju", "12345-010", "Cidade Vinsmoke Reiju", "Estado Vinsmoke Reiju"))
                );
        }

        public FornecedorModel BuscarPorId(int id)
        {
            foreach (FornecedorModel fornecedor in Fornecedores_DB)
            {
                if (fornecedor.IdFornecedor == id)
                {
                    return fornecedor;
                }
            }
            return null;
        }

        public FornecedorModel BuscarPorNome(string nome)
        {
            foreach (FornecedorModel fornecedor in Fornecedores_DB)
            {
                if (fornecedor.NomeEmpresa == nome)
                {
                    return fornecedor;
                }
            }
            return null;
        }

        public IEnumerable<FornecedorModel> BuscarTodos()
        {
            return Fornecedores_DB;
        }

        public void Incluir(FornecedorModel obj)
        {
            Fornecedores_DB.Add(obj);
        }

        public void Excluir(FornecedorModel obj)
        {
            Fornecedores_DB.Remove(obj);
        }

        public void Alterar(FornecedorModel obj)
        {
            foreach (FornecedorModel fornecedor in Fornecedores_DB)
            {
                if (fornecedor.IdFornecedor == obj.IdFornecedor)
                {
                    Fornecedores_DB.Remove(fornecedor);
                    Fornecedores_DB.Add(obj);
                }
            }

        }
    }
}
