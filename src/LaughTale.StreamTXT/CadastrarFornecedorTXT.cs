﻿using LaughTale.Modelos;
using System.Collections.Generic;
using LaughTale.DAL.Fornecedores;

namespace LaughTale.StreamTXT
{
    class CadastrarFornecedorTXT
    {
        public static void CadastrarFornecedorNoDB(List<string> listaDados)
        {
            FornecedorRepository fornecedorDao = new FornecedorRepository();

            foreach (var linha in listaDados)
            {
                string[] campos = linha.Split(',');

                string nomeEmpresa = campos[0];
                string cnpj = campos[1];
                string emailContato = campos[2];
                string telefoneContato = campos[3];
                string logradouro = campos[4];
                string numero = campos[5];
                string complemento = campos[6];
                string bairro = campos[7];
                string cep = campos[8];
                string cidade = campos[9];
                string estado = campos[10];
                EnderecoModel endereco = new EnderecoModel(logradouro, numero, complemento, bairro, cep, cidade, estado);
                FornecedorModel novoFornecedor = new FornecedorModel(nomeEmpresa, cnpj, emailContato, telefoneContato, endereco);

                fornecedorDao.Incluir(novoFornecedor);

            }
        }
    }
}
