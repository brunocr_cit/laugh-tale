﻿using System.Collections.Generic;
using System.IO;

namespace LaughTale.StreamTXT
{
    public class LeitorDeArquivoTXT
    {
        public static List<string> UsarStreamReader(string enderecoDoArquivo)
        {
            var listaLinhas = new List<string>();

            using (var fluxoDeArquivo = new FileStream(enderecoDoArquivo, FileMode.Open))
            using (var leitor = new StreamReader(fluxoDeArquivo))
            {
                while (!leitor.EndOfStream)
                {
                    var linha = leitor.ReadLine();
                    listaLinhas.Add(linha);
                }
            }

            return listaLinhas;
        }
    }
}
