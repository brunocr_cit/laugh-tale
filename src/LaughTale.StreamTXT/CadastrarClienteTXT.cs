﻿using LaughTale.DAL.Clientes;
using LaughTale.Modelos;
using System.Collections.Generic;

namespace LaughTale.StreamTXT
{
    class CadastrarClienteTXT
    {
        public static void CadastrarClienteNoDB(List<string> listaDados)
        {
            ClienteRepository clienteDao = new ClienteRepository();

            foreach (var linha in listaDados)
            {
                string[] campos = linha.Split(',');

                string nomeCompleto = campos[0];
                string nomeDaMae = campos[1];
                string cpf = campos[2];
                string telefoneContato = campos[3];
                string logradouro = campos[4];
                string numero = campos[5];
                string complemento = campos[6];
                string bairro = campos[7];
                string cep = campos[8];
                string cidade = campos[9];
                string estado = campos[10];
                var endereco = new EnderecoModel(logradouro, numero, complemento, bairro, cep, cidade, estado);
                var pessoa = new PessoaModel(nomeCompleto, nomeDaMae, cpf, telefoneContato, endereco);
                var novoCliente = new ClienteModel(pessoa);

                clienteDao.Incluir(novoCliente);

            }
        }
    }
}
