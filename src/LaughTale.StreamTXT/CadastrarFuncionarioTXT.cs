﻿using LaughTale.DAL.Funcionarios;
using LaughTale.Modelos;
using System.Collections.Generic;

namespace LaughTale.StreamTXT
{
    public class CadastrarFuncionarioTXT
    {
        public static void CadastrarFuncionarioNoDB(List<string> listaDados)
        {
            FuncionarioRepository funcionarioDao = new FuncionarioRepository();

            foreach (var linha in listaDados)
            {
                string[] campos = linha.Split(',');

                string nomeCompleto = campos[0];
                string nomeDaMae = campos[1];
                string cpf = campos[2];
                string telefoneContato = campos[3];
                string logradouro = campos[4];
                string numero = campos[5];
                string complemento = campos[6];
                string bairro = campos[7];
                string cep = campos[8];
                string cidade = campos[9];
                string estado = campos[10];
                var endereco = new EnderecoModel(logradouro, numero, complemento, bairro, cep, cidade, estado);
                PessoaModel pessoa = new PessoaModel(nomeCompleto, nomeDaMae, cpf, telefoneContato, endereco);
                string cargo = campos[11];
                string pis = campos[12];
                double salario = double.Parse(campos[13].Replace('.', ','));
                string senha = campos[14];


                FuncionarioModel novoFuncionario = new FuncionarioModel(pessoa, cargo, pis, salario, senha);

                funcionarioDao.Incluir(novoFuncionario);

            }
        }
    }
}
