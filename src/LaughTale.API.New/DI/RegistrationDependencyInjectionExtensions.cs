﻿using LaughTale.API.Mapper;
using LaughTale.API.Repository;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Service;
using LaughTale.API.Service.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LaughTale.API.DI
{
    public static class RegistrationDependencyInjectionExtensions
    {
        public static void AddRegistrationDependencies(this IServiceCollection services, IConfiguration configuration = null)
        {
            services.AddSingleton(configuration);

            RegisterOptions(services, configuration);
            RegisterInfra(services, configuration);
            RegisterRepositories(services, configuration);
            RegisterServices(services);
            RegisterValidators(services);
            services.AddSingleton<IObjectConverter, ObjectConverter>();
        }

        private static void RegisterRepositories(IServiceCollection services, IConfiguration configuration = null)
        {
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IFornecedorRepository, FornecedorRepository>();
            services.AddScoped<IFuncionarioRepository, FuncionarioRepository>();
            services.AddScoped<IItens_VendaRepository, ItensVendaRepository>();
            services.AddScoped<IVendaRepository, VendaRepository>();
            services.AddScoped<IVestuarioRepository, VestuarioRepository>();
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IClienteService, ClienteService>();
            services.AddScoped<IFornecedorService, FornecedorService>();
            services.AddScoped<IFuncionarioService, FuncionarioService>();
            //services.AddScoped<IItens_VendaService, Itens_VendaService>();
            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IVestuarioService, VestuarioService>();
            services.AddScoped<ICriarVendaService, CriarVendaService>();
            services.AddScoped<IRemoverItensCompradosDoEstoqueService, RemoverItensCompradosDoEstoqueService>();
            services.AddScoped<IConverterListaItensVendaSimplificadaParaItensVendaService, ConverterListaItensVendaSimplificadaParaItensVendaService>();
        }
        private static void RegisterOptions(IServiceCollection services, IConfiguration configuration)
        {
        }
        private static void RegisterValidators(IServiceCollection services)
        {
        }

        private static void RegisterInfra(IServiceCollection services, IConfiguration configuration)
        {
        }
    }
}
