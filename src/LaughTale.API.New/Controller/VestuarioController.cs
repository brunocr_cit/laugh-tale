﻿using LaughTale.API.Model;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Service;
using LaughTale.API.Mapper;
using LaughTale.API.ViewModel;
using Microsoft.AspNetCore.Http;
using LaughTale.API.Service.Interface;
using LaughTale.API.Request;
using System.Collections.Generic;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class VestuarioController : ControllerBase
    {
        private readonly IVestuarioService _vestuarioService;
        private readonly IObjectConverter _objectConverter;

        public VestuarioController(IObjectConverter objectConverter,
                                 IVestuarioService vestuarioService)
        {
            _vestuarioService = vestuarioService;
            _objectConverter = objectConverter;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de vestuarios.",
            Tags = new[] { "Vestuario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginarModel<VestuarioViewModel>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]

        public IActionResult ObterListaVestuarios(
            //[FromQuery] OrdenarModel ordem,
            [FromQuery] Paginacao paginacao)
        {
            var response = _vestuarioService.ObterListaVestuarios();

            var listaPaginada = PaginarModel<VestuarioViewModel>.From(paginacao, _objectConverter.Map<List<VestuarioViewModel>>(response));

            return response.Count == 0 ? NotFound() : (IActionResult)Ok(listaPaginada);
        }

        [HttpGet("identificador/{id}")]
        [SwaggerOperation(
            Summary = "Recupera o vestuario identificado por seu {id}.",
            Tags = new[] { "Vestuario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VestuarioViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterVestuarioPorId(
            [SwaggerParameter("Id do vestuario.", Required = true)] int id)
        {
            var model = _objectConverter.Map<VestuarioViewModel>(_vestuarioService.ObterVestuarioPorId(id));

            return model == null ? NotFound() : (IActionResult)Ok(model);
        }

        [HttpGet("descricao/{descricao}")]
        [SwaggerOperation(
            Summary = "Recupera o vestuario identificado por sua {descricao}.",
            Tags = new[] { "Vestuario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VestuarioViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterVestuarioPorDescricao(
            [SwaggerParameter("Descrição do vestuario.", Required = true)] string descricao)
        {
            var response = _objectConverter.Map<List<VestuarioViewModel>>(_vestuarioService.ObterVestuarioPorDescricao(descricao));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo vestuario na base.",
            Tags = new[] { "Vestuario" }
            )]
        //[Produces("application/json", "application/xml")]

        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VestuarioViewModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult RegistrarVestuario([FromBody] VestuarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<VestuarioRequest>(model);
                var response = _objectConverter.Map<VestuarioViewModel>(
                    _vestuarioService.RegistrarVestuario(transformationViewToRequest));
                var uri = Url.Action("ObterVestuarioPorId", new { id = response.IdVestuario });
                return Created(uri, response);
            }
            return BadRequest(ErroResponseModel.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o vestuario na base.",
            Tags = new[] { "Vestuario" })]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult AlterarVestuario([FromBody] VestuarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<VestuarioRequest>(model);
                _vestuarioService.AlterarVestuario(transformationViewToRequest);
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut("estoque/")]
        [SwaggerOperation(
            Summary = "Acrescenta unidades no estoque de um vestuario já cadastrado na base.",
            Tags = new[] { "Vestuario" })]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult AcrescentarNoEstoque([FromBody] AcrescentarVestuarioNoEstoqueViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<AlterarEstoqueDoVestuarioRequest>(model);
                _vestuarioService.AlterarEstoqueDoVestuario(transformationViewToRequest);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o vestuario da base.",
            Tags = new[] { "Vestuario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult Remover(int id)
        {
            return _vestuarioService.DeletarVestuario(id) == true ? (IActionResult)NoContent() : NotFound();
        }
    }
}