﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Model;
using LaughTale.API.Service;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using LaughTale.API.Mapper;
using LaughTale.API.ViewModel;
using LaughTale.API.Request;
using LaughTale.API.Service.Interface;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteService _clienteService;
        private readonly IObjectConverter _objectConverter;

        public ClienteController(IObjectConverter objectConverter,
                                 IClienteService clienteService)
        {
            _clienteService = clienteService;
            _objectConverter = objectConverter;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de clientes.",
            Tags = new[] { "Cliente" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginarModel<ClienteViewModel>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]

        public IActionResult ObterListaClientes(
            //[FromQuery] OrdenarModel ordem,
            [FromQuery] Paginacao paginacao)
        {
            var response = _clienteService.ObterListaClientes();
            
            var listaPaginada = PaginarModel<ClienteViewModel>.From(paginacao, _objectConverter.Map<List<ClienteViewModel>>(response));

            return response.Count == 0 ? NotFound() : (IActionResult)Ok(listaPaginada);
        }

        [HttpGet("identificador/{id}")]
        [SwaggerOperation(
            Summary = "Recupera o cliente identificado por seu {id}.",
            Tags = new[] { "Cliente" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterClientePorId(
            [SwaggerParameter("Id do cliente.", Required = true)] int id)
        {
            var model = _objectConverter.Map<ClienteViewModel>(_clienteService.ObterClientePorId(id));

            return model == null ? NotFound() : (IActionResult)Ok(model);
        }

        [HttpGet("nome/{nome}")]
        [SwaggerOperation(
            Summary = "Recupera o cliente identificado por seu {nome}.",
            Tags = new[] { "Cliente" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterClientePorNome(
            [SwaggerParameter("Nome do cliente.", Required = true)] string nome)
        {
            var response = _objectConverter.Map<List<ClienteViewModel>>(_clienteService.ObterClientePorNome(nome));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }
        
        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo cliente na base.",
            Tags = new[] { "Cliente" }
            )]
        //[Produces("application/json", "application/xml")]

        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ClienteViewModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult RegistrarCliente([FromBody] ClienteViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<ClienteRequest>(model);
                var response = _objectConverter.Map<ClienteViewModel>(
                    _clienteService.RegistrarCliente(transformationViewToRequest));
                var uri = Url.Action("ObterClientePorId", new { id = response.IdCliente});
                return Created(uri, response);
            }
            return BadRequest(ErroResponseModel.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o cliente na base.",
            Tags = new[] { "Cliente" })]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult AlterarCliente([FromBody] ClienteViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<ClienteRequest>(model);
                _clienteService.AlterarCliente(transformationViewToRequest);
                return Ok(); 
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o cliente da base.",
            Tags = new[] { "Cliente" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult Remover(int id)
        {
            return _clienteService.DeletarCliente(id) == true ? (IActionResult)NoContent() : NotFound();
        }
    }
}
