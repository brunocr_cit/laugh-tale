﻿using LaughTale.API.Model;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Service;
using System.Collections.Generic;
using LaughTale.API.Service.Interface;
using LaughTale.API.Mapper;
using Microsoft.AspNetCore.Http;
using LaughTale.API.ViewModel;
using LaughTale.API.Request;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class FuncionarioController : ControllerBase
    {
        private readonly IFuncionarioService _funcionarioService;
        private readonly IObjectConverter _objectConverter;

        public FuncionarioController(IObjectConverter objectConverter,
                                 IFuncionarioService funcionarioService)
        {
            _funcionarioService = funcionarioService;
            _objectConverter = objectConverter;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de funcionarios.",
            Tags = new[] { "Funcionario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginarModel<FuncionarioViewModel>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]

        public IActionResult ObterListaFuncionarios(
            //[FromQuery] OrdenarModel ordem,
            [FromQuery] Paginacao paginacao)
        {
            var response = _funcionarioService.ObterListaFuncionarios();

            var listaPaginada = PaginarModel<FuncionarioViewModel>.From(paginacao, _objectConverter.Map<List<FuncionarioViewModel>>(response));

            return response.Count == 0 ? NotFound() : (IActionResult)Ok(listaPaginada);
        }

        [HttpGet("identificador/{id}")]
        [SwaggerOperation(
            Summary = "Recupera o funcionario identificado por seu {id}.",
            Tags = new[] { "Funcionario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FuncionarioViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterFuncionarioPorId(
            [SwaggerParameter("Id do funcionario.", Required = true)] int id)
        {
            var model = _objectConverter.Map<FuncionarioViewModel>(_funcionarioService.ObterFuncionarioPorId(id));

            return model == null ? NotFound() : (IActionResult)Ok(model);
        }

        [HttpGet("nome/{nome}")]
        [SwaggerOperation(
            Summary = "Recupera o funcionario identificado por seu {nome}.",
            Tags = new[] { "Funcionario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FuncionarioViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterFuncionarioPorNome(
            [SwaggerParameter("Nome do funcionario.", Required = true)] string nome)
        {
            var response = _objectConverter.Map<List<FuncionarioViewModel>>(_funcionarioService.ObterFuncionarioPorNome(nome));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo funcionario na base.",
            Tags = new[] { "Funcionario" }
            )]
        //[Produces("application/json", "application/xml")]

        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(FuncionarioViewModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult RegistrarFuncionario([FromBody] FuncionarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<FuncionarioRequest>(model);
                var response = _objectConverter.Map<FuncionarioViewModel>(
                    _funcionarioService.RegistrarFuncionario(transformationViewToRequest));
                var uri = Url.Action("ObterFuncionarioPorId", new { id = response.IdFuncionario });
                return Created(uri, response);
            }
            return BadRequest(ErroResponseModel.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o funcionario na base.",
            Tags = new[] { "Funcionario" })]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult AlterarFuncionario([FromBody] FuncionarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<FuncionarioRequest>(model);
                _funcionarioService.AlterarFuncionario(transformationViewToRequest);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o funcionario da base.",
            Tags = new[] { "Funcionario" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult Remover(int id)
        {
            return _funcionarioService.DeletarFuncionario(id) == true ? (IActionResult)NoContent() : NotFound();
        }
    }
}
