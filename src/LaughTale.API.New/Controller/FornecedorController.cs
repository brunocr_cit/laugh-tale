﻿using LaughTale.API.Model;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Service;
using System.Collections.Generic;
using LaughTale.API.Service.Interface;
using LaughTale.API.Mapper;
using Microsoft.AspNetCore.Http;
using LaughTale.API.ViewModel;
using LaughTale.API.Request;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class FornecedorController : ControllerBase
    {
        private readonly IFornecedorService _fornecedorService;
        private readonly IObjectConverter _objectConverter;

        public FornecedorController(IObjectConverter objectConverter,
                                 IFornecedorService fornecedorService)
        {
            _fornecedorService = fornecedorService;
            _objectConverter = objectConverter;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de fornecedores.",
            Tags = new[] { "Fornecedor" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginarModel<FornecedorViewModel>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]

        public IActionResult ObterListaFornecedores(
            //[FromQuery] OrdenarModel ordem,
            [FromQuery] Paginacao paginacao)
        {
            var response = _fornecedorService.ObterListaFornecedores();

            var listaPaginada = PaginarModel<FornecedorViewModel>.From(paginacao, _objectConverter.Map<List<FornecedorViewModel>>(response));

            return response.Count == 0 ? NotFound() : (IActionResult)Ok(listaPaginada);
        }

        [HttpGet("identificador/{id}")]
        [SwaggerOperation(
            Summary = "Recupera o fornecedor identificado por seu {id}.",
            Tags = new[] { "Fornecedor" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FornecedorViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterFornecedorPorId(
            [SwaggerParameter("Id do fornecedor.", Required = true)] int id)
        {
            var model = _objectConverter.Map<FornecedorViewModel>(_fornecedorService.ObterFornecedorPorId(id));

            return model == null ? NotFound() : (IActionResult)Ok(model);
        }

        [HttpGet("nome/{nome}")]
        [SwaggerOperation(
            Summary = "Recupera o fornecedor identificado por seu {nome}.",
            Tags = new[] { "Fornecedor" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FornecedorViewModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterFornecedorPorNome(
            [SwaggerParameter("Nome do fornecedor.", Required = true)] string nome)
        {
            var response = _objectConverter.Map<List<FornecedorViewModel>>(_fornecedorService.ObterFornecedorPorNome(nome));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo fornecedor na base.",
            Tags = new[] { "Fornecedor" }
            )]
        //[Produces("application/json", "application/xml")]

        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(FornecedorViewModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult RegistrarFornecedor([FromBody] FornecedorViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<FornecedorRequest>(model);
                var response = _objectConverter.Map<FornecedorViewModel>(
                    _fornecedorService.RegistrarFornecedor(transformationViewToRequest));
                var uri = Url.Action("ObterFornecedorPorId", new { id = response.IdFornecedor });
                return Created(uri, response);
            }
            return BadRequest(ErroResponseModel.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o fornecedor na base.",
            Tags = new[] { "Fornecedor" })]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult AlterarFornecedor([FromBody] FornecedorViewModel model)
        {
            if (ModelState.IsValid)
            {
                var transformationViewToRequest = _objectConverter.Map<FornecedorRequest>(model);
                _fornecedorService.AlterarFornecedor(transformationViewToRequest);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o fornecedor da base.",
            Tags = new[] { "Fornecedor" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult Remover(int id)
        {
            return _fornecedorService.DeletarFornecedor(id) == true ? (IActionResult)NoContent() : NotFound();
        }
    }
}
