﻿using LaughTale.API.Model;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Service;
using Microsoft.AspNetCore.Http;
using LaughTale.API.Mapper;
using LaughTale.API.ViewModel;
using System.Collections.Generic;
using LaughTale.API.Request;
using LaughTale.API.Service.Interface;
using LaughTale.API.Response;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _vendaService;
        private readonly IObjectConverter _objectConverter;

        public VendaController(IObjectConverter objectConverter,
                                 IVendaService vendaService)
        {
            _vendaService = vendaService;
            _objectConverter = objectConverter;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de vendas.",
            Tags = new[] { "Venda" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaginarModel<VendaViewModelResponse>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]

        public IActionResult ObterListaVendas(
            //[FromQuery] OrdenarModel ordem,
            [FromQuery] Paginacao paginacao)
        {
            var response = _vendaService.ObterListaVendas();

            var listaPaginada = PaginarModel<VendaViewModelResponse>.From(paginacao, _objectConverter.Map<List<VendaViewModelResponse>>(response));

            return response.Count == 0 ? NotFound() : (IActionResult)Ok(listaPaginada);
        }

        [HttpGet("identificador/{id}")]
        [SwaggerOperation(
            Summary = "Recupera o venda identificado por seu {id}.",
            Tags = new[] { "Venda" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VendaViewModelResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterVendaPorId(
            [SwaggerParameter("Id do venda.", Required = true)] int id)
        {
            var model = _objectConverter.Map<VendaViewModelResponse>(_vendaService.ObterVendaPorId(id));

            return model == null ? NotFound() : (IActionResult)Ok(model);
        }

        [HttpGet("nomecliente/{nome}")]
        [SwaggerOperation(
            Summary = "Recupera as compras realizadas por um cliente, identificado por seu {nome}.",
            Tags = new[] { "Venda" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VendaViewModelResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterVendaPorNomeCliente(
            [SwaggerParameter("Nome do venda.", Required = true)] string nomeCompleto, string nomeMae)
        {
            var response = _objectConverter.Map<List<VendaViewModelResponse>>(_vendaService.ObterVendaPorNomeCliente(nomeCompleto, nomeMae));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }

        [HttpGet("nomefuncionario/{nome}")]
        [SwaggerOperation(
            Summary = "Recupera as vendas realizadas por um funcionario, identificado por seu {nome}.",
            Tags = new[] { "Venda" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VendaViewModelResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult ObterVendaPorNomeFuncionario(
            [SwaggerParameter("Nome do venda.", Required = true)] string nome)
        {
            var response = _objectConverter.Map<List<VendaViewModelResponse>>(_vendaService.ObterVendaPorNomeFuncionario(nome));
            return response.Count == 0 ? NotFound() : (IActionResult)Ok(response);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo venda na base.",
            Tags = new[] { "Venda" }
            )]
        //[Produces("application/json", "application/xml")]

        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VendaViewModelResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult RegistrarVenda([FromBody] VendaViewModelRequest model)
        {
            if (ModelState.IsValid)
            {
                VendaRequest transformationViewToRequest = _objectConverter.Map<VendaRequest>(model);
                VendaResponse response = _vendaService.RegistrarVenda(transformationViewToRequest);
                var uri = Url.Action("ObterVendaPorId", new { id = response.IdVenda });
                return Created(uri, response);
            }
            return BadRequest(ErroResponseModel.FromModelStateError(ModelState));
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o venda da base.",
            Tags = new[] { "Venda" }
        )]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponseModel))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponseModel))]
        public IActionResult Remover(int id)
        {
            return _vendaService.DeletarVenda(id) == true ? (IActionResult)NoContent() : NotFound();
        }
    }
}
