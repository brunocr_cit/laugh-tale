﻿using LaughTale.API.Mapper.Profiles;
using AutoMapper;

namespace LaughTale.API.Mapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings(params Profile[] profiles)
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ModelToRequestProfile());
                cfg.AddProfile(new ModelToResponseProfile());
                cfg.AddProfile(new RequestToModelProfile());
                cfg.AddProfile(new ResponseToModelProfile());
                cfg.AddProfile(new ResponseToRequestProfile());
                cfg.AddProfile(new ResponseToViewModelProfile());
                cfg.AddProfile(new ViewModelToRequestProfile());
            });
        }
    }
}
