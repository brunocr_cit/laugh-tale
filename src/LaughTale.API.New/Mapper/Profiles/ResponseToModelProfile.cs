﻿using LaughTale.API.Model;
using LaughTale.API.Response;
using AutoMapper;

namespace LaughTale.API.Mapper.Profiles
{
    public class ResponseToModelProfile : Profile
    {
        public ResponseToModelProfile()
        {
            CreateMap<ClienteResponse, ClienteModel>()
               .ForMember(dest => dest.IdCliente, opt => opt.MapFrom(src => src.IdCliente))
               .ForMember(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForMember(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForMember(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForMember(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Endereco.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Endereco.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Endereco.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Endereco.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.Endereco.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Endereco.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Endereco.Estado, opt => opt.MapFrom(src => src.Estado));

            CreateMap<FuncionarioResponse, FuncionarioModel>().ForMember(dest => dest.Cargo, opt => opt.MapFrom(src => src.Cargo))
               .ForMember(dest => dest.PIS, opt => opt.MapFrom(src => src.PIS))
               .ForMember(dest => dest.Salario, opt => opt.MapFrom(src => src.Salario))
               .ForMember(dest => dest.Senha, opt => opt.MapFrom(src => src.Senha))
               .ForMember(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForMember(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForMember(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForMember(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Endereco.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Endereco.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Endereco.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Endereco.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.Endereco.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Endereco.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Endereco.Estado, opt => opt.MapFrom(src => src.Estado));
        }
    }
}
