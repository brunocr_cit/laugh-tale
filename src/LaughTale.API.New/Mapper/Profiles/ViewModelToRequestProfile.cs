﻿using AutoMapper;
using LaughTale.API.Request;
using LaughTale.API.ViewModel;

namespace LaughTale.API.Mapper.Profiles
{
    public class ViewModelToRequestProfile : Profile
    {
        public ViewModelToRequestProfile()
        {
            CreateMap<ClienteViewModel, ClienteRequest>();

            CreateMap<FornecedorViewModel, FornecedorRequest>();

            CreateMap<FuncionarioViewModel, FuncionarioRequest>();

            CreateMap<VendaViewModelRequest, VendaRequest>();

            CreateMap<VestuarioViewModel, VestuarioRequest>();

            CreateMap<AcrescentarVestuarioNoEstoqueViewModel, AlterarEstoqueDoVestuarioRequest>();
        }
    }
}
