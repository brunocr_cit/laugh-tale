﻿using AutoMapper;
using LaughTale.API.Response;
using LaughTale.API.Model;

namespace LaughTale.API.Mapper.Profiles
{
    public class ModelToResponseProfile : Profile
    {
        public ModelToResponseProfile()
        {
            CreateMap<ClienteModel, ClienteResponse>()
               .ForMember(dest => dest.IdCliente, opt => opt.MapFrom(src => src.IdCliente))
               .ForMember(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForMember(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForMember(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForMember(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForMember(dest => dest.Logradouro, opt => opt.MapFrom(src => src.Endereco.Logradouro))
               .ForMember(dest => dest.Numero, opt => opt.MapFrom(src => src.Endereco.Numero))
               .ForMember(dest => dest.Complemento, opt => opt.MapFrom(src => src.Endereco.Complemento))
               .ForMember(dest => dest.Bairro, opt => opt.MapFrom(src => src.Endereco.Bairro))
               .ForMember(dest => dest.CEP, opt => opt.MapFrom(src => src.Endereco.CEP))
               .ForMember(dest => dest.Cidade, opt => opt.MapFrom(src => src.Endereco.Cidade))
               .ForMember(dest => dest.Estado, opt => opt.MapFrom(src => src.Endereco.Estado));

            CreateMap<FornecedorModel, FornecedorResponse>()
               .ForMember(dest => dest.IdFornecedor, opt => opt.MapFrom(src => src.IdFornecedor))
               .ForMember(dest => dest.NomeEmpresa, opt => opt.MapFrom(src => src.NomeEmpresa))
               .ForMember(dest => dest.CNPJ, opt => opt.MapFrom(src => src.CNPJ))
               .ForMember(dest => dest.EmailContato, opt => opt.MapFrom(src => src.EmailContato))
               .ForMember(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Logradouro, opt => opt.MapFrom(src => src.Endereco.Logradouro))
               .ForPath(dest => dest.Numero, opt => opt.MapFrom(src => src.Endereco.Numero))
               .ForPath(dest => dest.Complemento, opt => opt.MapFrom(src => src.Endereco.Complemento))
               .ForPath(dest => dest.Bairro, opt => opt.MapFrom(src => src.Endereco.Bairro))
               .ForPath(dest => dest.CEP, opt => opt.MapFrom(src => src.Endereco.CEP))
               .ForPath(dest => dest.Cidade, opt => opt.MapFrom(src => src.Endereco.Cidade))
               .ForPath(dest => dest.Estado, opt => opt.MapFrom(src => src.Endereco.Estado));


            CreateMap<FuncionarioModel, FuncionarioResponse>().ForMember(dest => dest.Cargo, opt => opt.MapFrom(src => src.Cargo))
               .ForMember(dest => dest.PIS, opt => opt.MapFrom(src => src.PIS))
               .ForMember(dest => dest.Salario, opt => opt.MapFrom(src => src.Salario))
               .ForMember(dest => dest.Senha, opt => opt.MapFrom(src => src.Senha))
               .ForPath(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForPath(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForPath(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForPath(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Logradouro, opt => opt.MapFrom(src => src.Endereco.Logradouro))
               .ForPath(dest => dest.Numero, opt => opt.MapFrom(src => src.Endereco.Numero))
               .ForPath(dest => dest.Complemento, opt => opt.MapFrom(src => src.Endereco.Complemento))
               .ForPath(dest => dest.Bairro, opt => opt.MapFrom(src => src.Endereco.Bairro))
               .ForPath(dest => dest.CEP, opt => opt.MapFrom(src => src.Endereco.CEP))
               .ForPath(dest => dest.Cidade, opt => opt.MapFrom(src => src.Endereco.Cidade))
               .ForPath(dest => dest.Estado, opt => opt.MapFrom(src => src.Endereco.Estado));

            CreateMap<VendaModel, VendaResponse>()
                .ForMember(dest => dest.IdVenda, opt => opt.MapFrom(src => src.IdVenda))
                .ForMember(dest => dest.CarroCompras, opt => opt.MapFrom(src => src.CarroCompras))
                .ForMember(dest => dest.DataVenda, opt => opt.MapFrom(src => src.DataVenda))
                .ForMember(dest => dest.FormaPagamento, opt => opt.MapFrom(src => src.FormaPagamento))
                .ForMember(dest => dest.ListaParcelas, opt => opt.MapFrom(src => src.ListaParcelas))
                .ForMember(dest => dest.ValorVenda, opt => opt.MapFrom(src => src.ValorVenda))
                .ForMember(dest => dest.ComissaoVenda, opt => opt.MapFrom(src => src.ComissaoVenda))
                .ForMember(dest => dest.IdVendedor, opt => opt.MapFrom(src => src.IdVendedor))
                .ForMember(dest => dest.IdCliente, opt => opt.MapFrom(src => src.IdCliente));

            CreateMap<VestuarioModel, VestuarioResponse>()
               .ForMember(dest => dest.Tamanho, opt => opt.MapFrom(src => src.Tamanho))
               .ForMember(dest => dest.CodigoBarras, opt => opt.MapFrom(src => src.CodigoBarras))
               .ForPath(dest => dest.EstoqueTotal, opt => opt.MapFrom(src => src.EstoqueTotal))
               .ForPath(dest => dest.IdMercadoria, opt => opt.MapFrom(src => src.IdVestuario))
               .ForPath(dest => dest.Descricao, opt => opt.MapFrom(src => src.Descricao))
               .ForPath(dest => dest.Quantidade, opt => opt.MapFrom(src => src.Quantidade))
               .ForPath(dest => dest.PrecoUnidadeFornecedor, opt => opt.MapFrom(src => src.PrecoUnidadeFornecedor))
               .ForPath(dest => dest.PrecoUnidadeCliente, opt => opt.MapFrom(src => src.PrecoUnidadeCliente))
               .ForPath(dest => dest.IdFornecedor, opt => opt.MapFrom(src => src.IdFornecedor))
               .ForPath(dest => dest.IdEstoquista, opt => opt.MapFrom(src => src.IdEstoquista));

        }
    }
}
