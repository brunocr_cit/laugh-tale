﻿using AutoMapper;
using LaughTale.API.Model;
using LaughTale.API.Request;

namespace LaughTale.API.Mapper.Profiles
{
    public class RequestToModelProfile : Profile
    {
        public RequestToModelProfile()
        {
            CreateMap<ClienteRequest, ClienteModel>()
               .ForPath(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForPath(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForPath(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForPath(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Endereco.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Endereco.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Endereco.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Endereco.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.Endereco.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Endereco.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Endereco.Estado, opt => opt.MapFrom(src => src.Estado));

            CreateMap<FornecedorRequest, FornecedorModel>()
               .ForMember(dest => dest.IdFornecedor, opt => opt.MapFrom(src => src.IdFornecedor))
               .ForMember(dest => dest.NomeEmpresa, opt => opt.MapFrom(src => src.NomeEmpresa))
               .ForMember(dest => dest.CNPJ, opt => opt.MapFrom(src => src.CNPJ))
               .ForMember(dest => dest.EmailContato, opt => opt.MapFrom(src => src.EmailContato))
               .ForMember(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Endereco.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Endereco.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Endereco.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Endereco.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.Endereco.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Endereco.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Endereco.Estado, opt => opt.MapFrom(src => src.Estado));
               

            CreateMap<FuncionarioRequest, FuncionarioModel>()
               .ForMember(dest => dest.IdFuncionario, opt => opt.MapFrom(src => src.IdFuncionario))
               .ForMember(dest => dest.Cargo, opt => opt.MapFrom(src => src.Cargo))
               .ForMember(dest => dest.PIS, opt => opt.MapFrom(src => src.PIS))
               .ForMember(dest => dest.Salario, opt => opt.MapFrom(src => src.Salario))
               .ForMember(dest => dest.Senha, opt => opt.MapFrom(src => src.Senha))
               .ForPath(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForPath(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForPath(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForPath(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Endereco.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Endereco.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Endereco.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Endereco.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.Endereco.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Endereco.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Endereco.Estado, opt => opt.MapFrom(src => src.Estado));

            CreateMap<VestuarioRequest, VestuarioModel>()
               .ForMember(dest => dest.Tamanho, opt => opt.MapFrom(src => src.Tamanho))
               .ForMember(dest => dest.CodigoBarras, opt => opt.MapFrom(src => src.CodigoBarras))
               .ForPath(dest => dest.EstoqueTotal, opt => opt.MapFrom(src => src.EstoqueTotal))
               .ForPath(dest => dest.IdVestuario, opt => opt.MapFrom(src => src.IdVestuario))
               .ForPath(dest => dest.Descricao, opt => opt.MapFrom(src => src.Descricao))
               .ForPath(dest => dest.Quantidade, opt => opt.MapFrom(src => src.Quantidade))
               .ForPath(dest => dest.PrecoUnidadeFornecedor, opt => opt.MapFrom(src => src.PrecoUnidadeFornecedor))
               .ForPath(dest => dest.PrecoUnidadeCliente, opt => opt.MapFrom(src => src.PrecoUnidadeCliente))
               .ForPath(dest => dest.IdFornecedor, opt => opt.MapFrom(src => src.IdFornecedor))
               .ForPath(dest => dest.IdEstoquista, opt => opt.MapFrom(src => src.IdEstoquista));

            CreateMap<AlterarEstoqueDoVestuarioRequest, AlterarEstoqueDoVestuarioModel>();
        }
    }
}
