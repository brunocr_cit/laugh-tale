﻿using AutoMapper;
using LaughTale.API.Response;
using LaughTale.API.Request;

namespace LaughTale.API.Mapper.Profiles
{
    public class ResponseToRequestProfile : Profile
    {
        public ResponseToRequestProfile()
        {
            CreateMap<FuncionarioResponse, FuncionarioRequest>()
               .ForMember(dest => dest.IdFuncionario, opt => opt.MapFrom(src => src.IdFuncionario))
               .ForMember(dest => dest.Cargo, opt => opt.MapFrom(src => src.Cargo))
               .ForMember(dest => dest.PIS, opt => opt.MapFrom(src => src.PIS))
               .ForMember(dest => dest.Salario, opt => opt.MapFrom(src => src.Salario))
               .ForMember(dest => dest.Senha, opt => opt.MapFrom(src => src.Senha))
               .ForPath(dest => dest.NomeCompleto, opt => opt.MapFrom(src => src.NomeCompleto))
               .ForPath(dest => dest.NomeDaMae, opt => opt.MapFrom(src => src.NomeDaMae))
               .ForPath(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF))
               .ForPath(dest => dest.TelefoneContato, opt => opt.MapFrom(src => src.TelefoneContato))
               .ForPath(dest => dest.Logradouro, opt => opt.MapFrom(src => src.Logradouro))
               .ForPath(dest => dest.Numero, opt => opt.MapFrom(src => src.Numero))
               .ForPath(dest => dest.Complemento, opt => opt.MapFrom(src => src.Complemento))
               .ForPath(dest => dest.Bairro, opt => opt.MapFrom(src => src.Bairro))
               .ForPath(dest => dest.CEP, opt => opt.MapFrom(src => src.CEP))
               .ForPath(dest => dest.Cidade, opt => opt.MapFrom(src => src.Cidade))
               .ForPath(dest => dest.Estado, opt => opt.MapFrom(src => src.Estado));
        }
    }
}
