﻿using AutoMapper;
using LaughTale.API.Response;
using LaughTale.API.ViewModel;

namespace LaughTale.API.Mapper.Profiles
{
    public class ResponseToViewModelProfile : Profile
    {
        public ResponseToViewModelProfile()
        {
            CreateMap<ClienteResponse, ClienteViewModel>();

            CreateMap<FornecedorResponse, FornecedorViewModel>();

            CreateMap<FuncionarioResponse, FuncionarioViewModel>();

            CreateMap<VendaResponse, VendaViewModelResponse>();

            CreateMap<VestuarioResponse, VestuarioViewModel>();
            
        }
    }
}
