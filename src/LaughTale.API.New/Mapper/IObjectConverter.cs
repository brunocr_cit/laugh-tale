﻿namespace LaughTale.API.Mapper
{
    public interface IObjectConverter
    {
        T Map<T>(object source);
    }
}
