﻿using LaughTale.API.Model;
using System;
using System.Collections.Generic;

namespace LaughTale.API.ViewModel
{
    public class VendaViewModelResponse
    {
        public int IdVenda { get; set; }
        public IList<ItensVendaModel> CarroCompras { get; set; }
        public DateTime DataVenda { get; set; }
        public string FormaPagamento { get; set; }
        public IList<ParcelaModel> ListaParcelas { get; set; }
        public double ValorVenda { get; set; }
        public double ComissaoVenda { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
        public List<DateTime> ListaVencimentoParcelas { get; set; }
        public List<DateTime> ListaParcelasVencidas { get; set; }
    }
}
