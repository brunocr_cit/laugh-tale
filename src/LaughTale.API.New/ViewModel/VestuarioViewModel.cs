﻿namespace LaughTale.API.ViewModel
{
    public class VestuarioViewModel
    {
        public int EstoqueTotal { get; set; }
        public int IdVestuario { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public double PrecoUnidadeFornecedor { get; set; }
        public double PrecoUnidadeCliente { get; set; }
        public int IdFornecedor { get; set; }
        public int IdEstoquista { get; set; }
        public string Tamanho { get; set; }

        public string CodigoBarras { get; set; }
    }
}
