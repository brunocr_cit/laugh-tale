﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.ViewModel
{
    public class VendaViewModelRequest
    {
        public List<ItensVendaSimplificadoModel> CarroCompras { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
        public double ValorVenda { get; set; }
        public string FormaPagamento { get; set; }
        public int QuantidadeParcelas { get; set; }

    }
}
