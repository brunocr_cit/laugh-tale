﻿namespace LaughTale.API.ViewModel
{
    public class AcrescentarVestuarioNoEstoqueViewModel
    {
        public int IdMercadoria { get; set; }
        public int QuantidadeASerAcrescentada { get; set; }
    }
}
