﻿namespace LaughTale.API.ViewModel
{
    public class FornecedorViewModel
    {
        public int IdFornecedor { get; set; }
        public string NomeEmpresa { get; set; }
        public string CNPJ { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        
    }
}
