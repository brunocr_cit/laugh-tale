﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace LaughTale.API.Filtros
{
    public class TagDescriptionsDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new[] 
            {
            new OpenApiTag { Name = "Cliente", Description = "Consulta e mantém os clientes." },
            new OpenApiTag { Name = "Fornecedor", Description = "Consulta e mantém os fornecedores." },
            new OpenApiTag { Name = "Funcionario", Description = "Consulta e mantém os funcionarios." },
            new OpenApiTag { Name = "Mercadoria", Description = "Consulta e mantém as mercadorias." },
            new OpenApiTag { Name = "Venda", Description = "Consulta e mantém as vendas." },
            new OpenApiTag { Name = "Item_Venda", Description = "Consulta e mantém os itens que compõem o carrinho de uma venda." }
            };
        }
    }
}
