﻿using System.ComponentModel.DataAnnotations;

namespace LaughTale.API.Seguranca
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Funcionário")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }
    }
}
