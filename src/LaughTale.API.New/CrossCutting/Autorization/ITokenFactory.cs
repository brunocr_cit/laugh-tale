﻿namespace LaughTale.API.Seguranca
{
    public interface ITokenFactory
    {
        string Token { get; }
    }
}
