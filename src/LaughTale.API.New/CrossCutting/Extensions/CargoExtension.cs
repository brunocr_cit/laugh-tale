﻿using LaughTale.API.Model;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Extensions
{
    public static class CargoExtension
    {
        private static Dictionary<string, ECargo> mapa = new Dictionary<string, ECargo>
        {
            { "Gerente", ECargo.Gerente},
            { "Vendedor", ECargo.Vendedor },
            { "Estoquista", ECargo.Estoquista }
        };

        public static string ParaString(this ECargo valor)
        {
            return mapa.First(c => c.Value == valor).Key;
        }

        public static ECargo ParaValor(this string texto)
        {
            return mapa.First(c => c.Key == texto).Value;
        }
    }
}
