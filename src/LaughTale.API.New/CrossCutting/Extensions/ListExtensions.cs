﻿using System.Collections.Generic;

namespace LaughTale.API.Extensions
{
    public static class ListExtentions
    {
        public static void AdicionarVarios<T>(this List<T> lista, params T[] itens)
        {
            foreach (T item in itens)
            {
                lista.Add(item);
            }
        }
    }
}
