﻿using LaughTale.API.Model;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Extensions
{
    public static class FormaPagamentoExtension
    {
        private static Dictionary<string, EFormasPagamento> mapa = new Dictionary<string, EFormasPagamento>
        {
            { "Cartão de Crédito", EFormasPagamento.CartaoCredito},
            { "Cartão de Débito", EFormasPagamento.CartaoDebito },
            { "Parcelamento no Crediário", EFormasPagamento.Crediario },
            { "Em Dinheiro", EFormasPagamento.Dinheiro }
        };

        public static string ParaString(this EFormasPagamento valor)
        {
            return mapa.First(c => c.Value == valor).Key;
        }

        public static EFormasPagamento ParaValor(this string texto)
        {
            return mapa.First(c => c.Key == texto).Value;
        }
    }
}
