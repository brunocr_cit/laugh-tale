﻿namespace LaughTale.API.Model
{
    public enum EFormasPagamento
    {
        Dinheiro,
        CartaoCredito,
        CartaoDebito,
        Crediario
    }
}
