﻿using System;

namespace LaughTale.API.Exceptions
{
    public class NaoEhPossivelAlterarUmaVendaException : Exception
    {
        public NaoEhPossivelAlterarUmaVendaException()
        {

        }
        public NaoEhPossivelAlterarUmaVendaException(string mensagem)
            : base(mensagem)
        {
        }

        public NaoEhPossivelAlterarUmaVendaException(string mensagem, Exception excecaoInterna)
            : base(mensagem, excecaoInterna)
        {

        }
    }
}
