﻿namespace LaughTale.API.Request
{
    public class AlterarEstoqueDoVestuarioRequest
    {
        public int IdMercadoria { get; set; }
        public int QuantidadeASerModificadaNoEstoque { get; set; }
    }
}
