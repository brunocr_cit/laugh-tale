﻿
using LaughTale.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaughTale.API.Request
{
    public class VendaRequest
    {
        public List<ItensVendaSimplificadoModel> CarroCompras { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
        public double ValorVenda { get; set; }
        public EFormasPagamento FormaPagamento { get; set; }
        public int QuantidadeParcelas { get; set; }

    }
}
