﻿namespace LaughTale.API.Request
{
    public class ClienteRequest
    {
        public int IdCliente { get; set; }
        public string NomeCompleto { get; set; }
        public string NomeDaMae { get; set; }
        public string CPF { get; set; }
        public string TelefoneContato { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}
