using LaughTale.API.DI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;

namespace LaughTale.API.New
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRegistrationDependencies(Configuration);
            services.AddControllers();

            //services.AddApiVersioning(options =>
            //{
            //    options.ReportApiVersions = true;
            //    options.AssumeDefaultVersionWhenUnspecified = true;
            //    options.ApiVersionReader = new QueryStringApiVersionReader();
            //    options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
            //});

            services.AddSwaggerGen(options => {

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);

                //options.DocInclusionPredicate((docName, apiDesc) =>
                //{
                //    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                //    var versions = methodInfo.DeclaringType
                //        .GetCustomAttributes(true)
                //        .OfType<ApiVersionAttribute>()
                //        .SelectMany(attr => attr.Versions);

                //    return versions.Any(v => $"v{v.ToString()}" == docName);
                //});

                //options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                //{
                //    Name = "Authorization",
                //    In = ParameterLocation.Header,
                //    Type = SecuritySchemeType.ApiKey,
                //    Description = "Autentica��o Bearer via JWT",
                //    Scheme = "Bearer"
                //});

                ////D�vida aqui
                //options.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //   {
                //       new OpenApiSecurityScheme
                //       {
                //           Reference = new OpenApiReference 
                //           {
                //               Type = ReferenceType.SecurityScheme,
                //               Id = "Bearer" 
                //           }
                //       }, new List<string>() 
                //   }
                //});

                options.EnableAnnotations();

                options.SwaggerDoc(
                    "v1.0",
                    new OpenApiInfo
                    {
                        Title = "Laugh Tale API",
                        Description = "API com servi�os relacionados ao cotidiano de uma loja comercial f�sica.",
                        Version = "1.0"
                    }
                );
            });

            //services.AddApiVersioning();

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = "JwtBearer";
            //    options.DefaultChallengeScheme = "JwtBearer";
            //}).AddJwtBearer("JwtBearer", options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = true,
            //        ValidateAudience = true,
            //        ValidateLifetime = true,
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("laughtale-webapi-authentication-valid")),
            //        ClockSkew = TimeSpan.FromMinutes(5),
            //        ValidIssuer = "LaughTale.WebApp",
            //        ValidAudience = "Postman",
            //    };
            //});

            services.AddCors();

            services.AddControllers().AddNewtonsoftJson();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors(builder => builder.WithOrigins("http://localhost"));

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Version 1.0");
                c.RoutePrefix = string.Empty;
            });

            //app.UseAuthentication();
        }
    }
}
