﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using System.Collections.Generic;

namespace LaughTale.API.Repository
{
    public class ItensVendaRepository: IItens_VendaRepository
    {
        //ToDo: Analisar a necessidade da existência dessa classe
        private static List<ItensVendaModel> ItensVendasDB { get; set; }

        public ItensVendaModel BuscarPorId(int id)
        {
            foreach (ItensVendaModel itens_venda in ItensVendasDB)
            {
                if (itens_venda.IdItensVenda == id)
                {
                    return itens_venda;
                }
            }
            return null;
        }

        public IEnumerable<ItensVendaModel> BuscarTodos()
        {
            return ItensVendasDB;
        }

        public void Incluir(ItensVendaModel obj)
        {
            ItensVendasDB.Add(obj);
        }

        public void Excluir(ItensVendaModel obj)
        {
            ItensVendasDB.Remove(obj);
        }

        public void Alterar(ItensVendaModel obj)
        {
            foreach (ItensVendaModel itens_venda in ItensVendasDB)
            {
                if (itens_venda.IdItensVenda == obj.IdItensVenda)
                {
                    ItensVendasDB.Remove(itens_venda);
                    ItensVendasDB.Add(obj);
                }
            }

        }
    }
}
