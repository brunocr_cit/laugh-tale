﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Configuration;
using Microsoft.EntityFrameworkCore;

namespace LaughTale.API.Repository.Base
{
    public class LaughTaleContext : DbContext 
    {
        public DbSet<ClienteModel> Clientes { get; set; }
        public DbSet<EnderecoModel> Enderecos { get; set; }
        public DbSet<FornecedorModel> Fornecedores { get; set; }
        public DbSet<FuncionarioModel> Funcionarios { get; set; }
        public DbSet<VestuarioModel> Vestuarios { get; set; }
        public DbSet<VendaModel> Vendas { get; set; }
        //public DbSet<CaixaRegistradoraModel> CaixaRegistradora { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string cs = "Server=localhost; Port=5432; Database=postgres; User Id=postgres; Password=123456";
            optionsBuilder.UseNpgsql(cs);
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ClienteConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EnderecoConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(FornecedorConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(FuncionarioConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(VestuarioConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(VendasConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ItensVendaConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ParcelaConfiguration).Assembly);
            //modelBuilder.ApplyConfigurationsFromAssembly(typeof(CaixaRegistradoraConfiguration).Assembly);
            

        }
    }
}
