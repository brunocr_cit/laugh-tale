﻿using LaughTale.API.Model;
using System.Collections.Generic;
using LaughTale.API.Repository.Interface;
using System.Linq;
using LaughTale.API.Repository.Base;
using LaughTale.API.Service;

namespace LaughTale.API.Repository
{
    //new VestuarioModel(100001, "Regatas masculinas de malha estampadas P", 10, 25, 50, fornecedorDao.BuscarPorId(1), funcionarioDao.BuscarPorId(6), "P"),
    //new VestuarioModel(100002, "Regatas masculinas de malha estampadas M", 10, 27.50, 55, fornecedorDao.BuscarPorId(1), funcionarioDao.BuscarPorId(6), "M"),
    //new VestuarioModel(100003, "Regatas masculinas de malha estampadas G", 10, 27.50, 55, fornecedorDao.BuscarPorId(1), funcionarioDao.BuscarPorId(6), "G"),
    //new VestuarioModel(100004, "Regatas masculinas de malha estampadas GG", 10, 27.50, 55, fornecedorDao.BuscarPorId(1), funcionarioDao.BuscarPorId(6), "GG"),
    //new VestuarioModel(100005, "Calças jeans com detalhes em rasgado 38", 10, 42.50, 85, fornecedorDao.BuscarPorId(2), funcionarioDao.BuscarPorId(6), "38"),
    //new VestuarioModel(100006, "Calças jeans com detalhes em rasgado 40", 10, 45, 90, fornecedorDao.BuscarPorId(2), funcionarioDao.BuscarPorId(6), "40")


    public class VestuarioRepository : IVestuarioRepository
    {
        private readonly LaughTaleContext _context;
        public VestuarioRepository()
        {
            _context = new LaughTaleContext();
        }
        public VestuarioModel BuscarPorId(int id)
        {
            return _context.Vestuarios.Where<VestuarioModel>(c => c.IdVestuario == id)
                .FirstOrDefault();
            
        }

        public IEnumerable<VestuarioModel> BuscarPorDescricao(string descricao)
        {
            return _context.Vestuarios.Where<VestuarioModel>(c => c.Descricao.Contains(descricao))
                .ToList();
        }

        public IEnumerable<VestuarioModel> BuscarTodos()
        {
            return _context.Vestuarios.ToList();
        }

        public void Incluir(VestuarioModel obj)
        {
            _context.Vestuarios.Add(ValidarDadosVestuarioService.ValidarDadosVestuario(obj));
            _context.SaveChanges();
        }

        public void Excluir(VestuarioModel model)
        {
            _context.Vestuarios.Remove(model);
            _context.SaveChanges();
        }

        public void Alterar(VestuarioModel obj)
        {
            var vestuarioAlterar = BuscarPorId(obj.IdVestuario);
            if (vestuarioAlterar != null)
            {
                _context.Vestuarios.Update(obj);
                _context.SaveChanges();
            }

        }

        public void AlterarEstoqueDoVestuario(AlterarEstoqueDoVestuarioModel acrescentarEstoque)
        {
            var vestuarioAlterar = BuscarPorId(acrescentarEstoque.IdMercadoria);
            if (vestuarioAlterar != null)
            {
                vestuarioAlterar.EstoqueTotal += acrescentarEstoque.QuantidadeASerAlterada;
                _context.Vestuarios.Update(vestuarioAlterar);
                _context.SaveChanges();
            }
        }
    }
}
