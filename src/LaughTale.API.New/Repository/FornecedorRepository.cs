﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Base;
using LaughTale.API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Repository
{
    //new FornecedorModel("Empresa Kureha", "12.345.678/1234-01", "empresakureha@contato.com", "(09) 9 9876-5432", new EnderecoModel("Rua Kureha", "009A", "Galpão 44B", "Bairro Kureha", "12345-009", "Cidade Kureha", "Estado Kureha")),
    //new FornecedorModel("Empresa Vinsmoke Reiju", "12.345.678/1234-02", "empresavinsmoke_reiju@contato.com", "(10) 9 9876-5432", new EnderecoModel("Rua Vinsmoke Reiju", "010A", "Galpão 23A", "Bairro Vinsmoke Reiju", "12345-010", "Cidade Vinsmoke Reiju", "Estado Vinsmoke Reiju"))

    public class FornecedorRepository : IFornecedorRepository
    {
        private readonly LaughTaleContext _context;

        public FornecedorRepository()
        {
            _context = new LaughTaleContext();
        }

        public FornecedorModel BuscarPorId(int id)
        {
            return _context.Fornecedores
                .Include(c => c.Endereco)
                .Where<FornecedorModel>(c => c.IdFornecedor == id)
                .FirstOrDefault();
        }

        public IEnumerable<FornecedorModel> BuscarPorNome(string nome)
        {
            return _context.Fornecedores
                .Include(c => c.Endereco)
                .Where<FornecedorModel>(c => c.NomeEmpresa.Contains(nome))
                .ToList();
        }

        public IEnumerable<FornecedorModel> BuscarTodos()
        {
            return _context.Fornecedores.Include(c => c.Endereco).ToList();
        }

        public void Incluir(FornecedorModel obj)
        {
            _context.Fornecedores.Add(obj);
            _context.SaveChanges();
        }

        public void Excluir(FornecedorModel model)
        {
            _context.Fornecedores.Remove(model);
            _context.SaveChanges();
        }

        public void Alterar(FornecedorModel obj)
        {
            var fornecedorAlterar = BuscarPorId(obj.IdFornecedor);
            if (fornecedorAlterar != null)
            {
                _context.Fornecedores.Update(obj);
                _context.SaveChanges();
            }
            
        }
    }
}
