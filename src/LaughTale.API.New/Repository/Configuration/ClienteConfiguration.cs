﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class ClienteConfiguration : PessoaConfiguration<ClienteModel>
    {
        public override void Configure(EntityTypeBuilder<ClienteModel> modelBuilder)
        {
            base.Configure(modelBuilder);

            modelBuilder
                .ToTable("cliente");
            modelBuilder
                .Property(c => c.IdCliente)
                .HasColumnName("idcliente");
            modelBuilder
                .HasKey(c => c.IdCliente);

        }
    }
}
