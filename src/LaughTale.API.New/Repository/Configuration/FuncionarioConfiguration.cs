﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class FuncionarioConfiguration : PessoaConfiguration<FuncionarioModel>
    {
        public override void Configure(EntityTypeBuilder<FuncionarioModel> modelBuilder)
        {
            base.Configure(modelBuilder);

            modelBuilder
                .ToTable("funcionario");
            modelBuilder
                .Property(c => c.IdFuncionario)
                .HasColumnName("idfuncionario");
            modelBuilder
                .HasKey(c => c.IdFuncionario);
            modelBuilder
                .Property(c => c.Cargo)
                .HasColumnName("cargo")
                .IsRequired();
            modelBuilder
                .Property(c => c.PIS)
                .HasColumnName("pis")
                .IsRequired();
            modelBuilder
                .Property(c => c.Salario)
                .HasColumnName("salario")
                .IsRequired();
            modelBuilder
                .Property(c => c.Senha)
                .HasColumnName("senha")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdEndereco)
                .HasColumnName("idendereco")
                .IsRequired();
            modelBuilder
                .HasOne<VestuarioModel>(f => f.Vestuario)
                .WithOne(v => v.Funcionario)
                .HasForeignKey<VestuarioModel>(v => v.IdEstoquista);
        }
    }
}
