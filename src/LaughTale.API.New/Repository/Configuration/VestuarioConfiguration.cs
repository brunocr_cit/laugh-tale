﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class VestuarioConfiguration : IEntityTypeConfiguration<VestuarioModel>
    {
        public void Configure(EntityTypeBuilder<VestuarioModel> modelBuilder)
        {
            modelBuilder
                .ToTable("vestuario");
            modelBuilder
                .Property(c => c.IdVestuario)
                .HasColumnName("idvestuario");
            modelBuilder
                .HasKey(c => c.IdVestuario);
            modelBuilder
                .Property(c => c.CodigoBarras)
                .HasColumnName("codigo_barras")
                .IsRequired();
            modelBuilder
                .Property(c => c.Descricao)
                .HasColumnName("descricao")
                .IsRequired();
            modelBuilder
                .Property(c => c.EstoqueTotal)
                .HasColumnName("estoque");
            modelBuilder
                .Property(c => c.Quantidade)
                .HasColumnName("quantidade_adicionada_por_ultimo")
                .IsRequired();
            modelBuilder
                .Property(c => c.PrecoUnidadeFornecedor)
                .HasColumnName("preco_unidade_fornecedor")
                .IsRequired();
            modelBuilder
                .Property(c => c.PrecoUnidadeCliente)
                .HasColumnName("preco_unidade_cliente")
                .IsRequired();
            modelBuilder
                .Property(c => c.Tamanho)
                .HasColumnName("tamanho")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdFornecedor)
                .HasColumnName("idfornecedor");
            modelBuilder
                .Property(c => c.IdEstoquista)
                .HasColumnName("idfuncionario");
        }
    }
}
