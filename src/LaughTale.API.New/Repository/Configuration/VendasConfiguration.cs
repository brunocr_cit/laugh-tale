﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class VendasConfiguration : IEntityTypeConfiguration<VendaModel>
    {
        public void Configure(EntityTypeBuilder<VendaModel> modelBuilder)
        {
            modelBuilder
                .ToTable("vendas");
            modelBuilder
                .Property(c => c.IdVenda)
                .HasColumnName("idvenda");
            modelBuilder
                .HasKey(c => c.IdVenda );
            modelBuilder
                .Property(c => c.ValorVenda)
                .HasColumnName("valor_venda")
                .IsRequired();
            modelBuilder
                .Property(c => c.DataVenda)
                .HasColumnName("data_venda")
                .IsRequired();
            modelBuilder
                .Property(c => c.ComissaoVenda)
                .HasColumnName("comissao_venda")
                .IsRequired();
            modelBuilder
                .Property(c => c.FormaPagamento)
                .HasColumnName("forma_pagamento")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdVendedor)
                .HasColumnName("idvendedor")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdCliente)
                .HasColumnName("idcliente")
                .IsRequired();
            modelBuilder
                .HasOne<FuncionarioModel>(e => e.Funcionario)
                .WithMany(c => c.VendasRealizadas)
                .HasForeignKey(c => c.IdVenda);
            modelBuilder
                .HasOne<ClienteModel>(e => e.Cliente)
                .WithMany(c => c.ComprasRealizadas)
                .HasForeignKey(c => c.IdVenda);
            modelBuilder
                .HasMany<ItensVendaModel>(e => e.CarroCompras)
                .WithOne(c => c.Venda)
                .HasForeignKey(c => c.IdVenda);
            modelBuilder
                .HasMany<ParcelaModel>(e => e.ListaParcelas)
                .WithOne(c => c.Venda)
                .HasForeignKey(c => c.IdVenda);
        }
    }
}
