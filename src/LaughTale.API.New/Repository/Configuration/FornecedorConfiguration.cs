﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class FornecedorConfiguration : IEntityTypeConfiguration<FornecedorModel>
    {
        public void Configure(EntityTypeBuilder<FornecedorModel> modelBuilder)
        {
            modelBuilder
                .ToTable("fornecedor");
            modelBuilder
                .Property(c => c.IdFornecedor)
                .HasColumnName("idfornecedor");
            modelBuilder
                .HasKey(c => c.IdFornecedor);
            modelBuilder
                .Property(c => c.NomeEmpresa)
                .HasColumnName("nome_empresa")
                .IsRequired();
            modelBuilder
                .Property(c => c.CNPJ)
                .HasColumnName("cnpj")
                .IsRequired();
            modelBuilder
                .Property(c => c.EmailContato)
                .HasColumnName("email_contato")
                .IsRequired();
            modelBuilder
                .Property(c => c.TelefoneContato)
                .HasColumnName("telefone_contato");
            modelBuilder
                .Property(c => c.IdEndereco)
                .HasColumnName("idendereco")
                .IsRequired();
            modelBuilder
                .HasOne<VestuarioModel>(f => f.Vestuario)
                .WithOne(v => v.Fornecedor)
                .HasForeignKey<VestuarioModel>(v => v.IdFornecedor);
        }
    }
}
