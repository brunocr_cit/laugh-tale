﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class EnderecoConfiguration : IEntityTypeConfiguration<EnderecoModel>
    {
        public void Configure(EntityTypeBuilder<EnderecoModel> modelBuilder)
        {
            modelBuilder
                .ToTable("endereco");
            modelBuilder
                .Property(c => c.IdEndereco)
                .HasColumnName("idendereco");
            modelBuilder
                .HasKey(c => c.IdEndereco);
            modelBuilder
                .Property(c => c.Logradouro)
                .HasColumnName("logradouro")
                .IsRequired();
            modelBuilder
                .Property(c => c.Numero)
                .HasColumnName("numero")
                .IsRequired();
            modelBuilder
                .Property(c => c.Complemento)
                .HasColumnName("complemento");
            modelBuilder
                .Property(c => c.Bairro)
                .HasColumnName("bairro")
                .IsRequired();
            modelBuilder
                .Property(c => c.CEP)
                .HasColumnName("cep")
                .IsRequired();
            modelBuilder
                .Property(c => c.Cidade)
                .HasColumnName("cidade")
                .IsRequired();
            modelBuilder
                .Property(c => c.Estado)
                .HasColumnName("estado")
                .IsRequired();
            modelBuilder
                .HasOne<ClienteModel>(e => e.Cliente)
                .WithOne(c => c.Endereco)
                .HasForeignKey<ClienteModel>(c => c.IdEndereco);
            modelBuilder
                .HasOne<FuncionarioModel>(e => e.Funcionario)
                .WithOne(c => c.Endereco)
                .HasForeignKey<FuncionarioModel>(c => c.IdEndereco);
            modelBuilder
                .HasOne<FornecedorModel>(e => e.Fornecedor)
                .WithOne(c => c.Endereco)
                .HasForeignKey<FornecedorModel>(c => c.IdEndereco);
        }
    }
}
