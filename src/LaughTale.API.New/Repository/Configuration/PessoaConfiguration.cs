﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class PessoaConfiguration<T> : IEntityTypeConfiguration<T> where T : PessoaModel
    {
        public virtual void Configure(EntityTypeBuilder<T> modelBuilder)
        {
            modelBuilder
                .Property(c => c.NomeCompleto)
                .HasColumnName("nome_completo")
                .IsRequired();
            modelBuilder
                .Property(c => c.NomeDaMae)
                .HasColumnName("nome_mae")
                .IsRequired();
            modelBuilder
                .Property(c => c.CPF)
                .HasColumnName("cpf")
                .IsRequired();
            modelBuilder
                .Property(c => c.TelefoneContato)
                .HasColumnName("telefone_contato")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdEndereco)
                .HasColumnName("idendereco")
                .IsRequired();
        }
    }
}
