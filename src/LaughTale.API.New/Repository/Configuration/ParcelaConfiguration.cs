﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class ParcelaConfiguration : IEntityTypeConfiguration<ParcelaModel>
    {
        public void Configure(EntityTypeBuilder<ParcelaModel> modelBuilder)
        {
            modelBuilder
                .ToTable("parcelas");
            modelBuilder
                .Property(c => c.IdParcela)
                .HasColumnName("idparcela");
            modelBuilder
                .HasKey(c => c.IdParcela);
            modelBuilder
                .Property(c => c.ValorParcela)
                .HasColumnName("valor_parcela")
                .IsRequired();
            modelBuilder
                .Property(c => c.StatusParcelaPaga)
                .HasColumnName("status_parcela_paga")
                .IsRequired();
            modelBuilder
                .Property(c => c.StatusParcelaVencida)
                .HasColumnName("status_parcela_vencida")
                .IsRequired();
            modelBuilder
                .Property(c => c.DataVencimentoParcela)
                .HasColumnName("data_vencimento_parcela")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdVenda)
                .HasColumnName("idvenda")
                .IsRequired();


        }
    }
}
