﻿using LaughTale.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LaughTale.API.Repository.Configuration
{
    public class ItensVendaConfiguration : IEntityTypeConfiguration<ItensVendaModel>
    {
        public void Configure(EntityTypeBuilder<ItensVendaModel> modelBuilder)
        {
            modelBuilder
                .ToTable("itens_venda");
            modelBuilder
                .Property(c => c.IdItensVenda)
                .HasColumnName("iditens_venda");
            modelBuilder
                .HasKey(c => c.IdItensVenda);
            modelBuilder
                .Property(c => c.QuantidadeItens)
                .HasColumnName("quantidade_itens")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdVestuario)
                .HasColumnName("idvestuario")
                .IsRequired();
            modelBuilder
                .Property(c => c.IdVenda)
                .HasColumnName("idvenda")
                .IsRequired();
            modelBuilder
                .HasOne<VestuarioModel>(v => v.Mercadoria)
                .WithOne(m => m.ItensVenda)
                .HasForeignKey<VestuarioModel>(v => v.IdVestuario);


        }
    }
}
