﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Base;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Service;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Repository
{
    //new FuncionarioModel(new PessoaModel("Boa Hancock", "Mãe Boa", "123456789-01", "(01) 9 9876-5432", new EnderecoModel("Rua Boa", "001A", "Casa", "Bairro Boa", "12345-001", "Cidade Boa", "Estado Boa")), "Gerente", "123.45678.90-1", 5000, "Ger_Boa"),
    //new FuncionarioModel(new PessoaModel("Shirahoshi", "Mãe Shirahoshi", "123456789-02", "(02) 9 9876-5432", new EnderecoModel("Rua Shirahoshi", "002A", "Apartamento 101", "Bairro Shirahoshi", "12345-002", "Cidade Shirahoshi", "Estado Shirahoshi")), "Gerente", "123.45678.90-2", 5000, "Ger_Shirahoshi"),
    //new FuncionarioModel(new PessoaModel("Nico Robin", "Mãe Nico", "123456789-03", "(03) 9 9876-5432", new EnderecoModel("Rua Nico", "003A", "Casa", "Bairro Nico", "12345-003", "Cidade Nico", "Estado Nico")), "Vendedor", "123.45678.90-3", 3000, "Ven_Nico"),
    //new FuncionarioModel(new PessoaModel("Nefertari Vivi", "Mãe Nefertari ", "123456789-04", "(04) 9 9876-5432", new EnderecoModel("Rua Nefertari ", "004A", "Apartamento 201", "Bairro Nefertari ", "12345-004", "Cidade Nefertari ", "Estado Nefertari ")), "Vendedor", "123.45678.90-4", 3000, "Ven_Nefertari "),
    //new FuncionarioModel(new PessoaModel("Nami", "Mãe Nami", "123456789-05", "(05) 9 9876-5432", new EnderecoModel("Rua Nami", "005A", "Casa", "Bairro Nami", "12345-005", "Cidade Nami", "Estado Nami")), "Vendedor", "123.45678.90-5", 3000, "Ven_Nami"),
    //new FuncionarioModel(new PessoaModel("Tama", "Mãe Tama", "123456789-11", "(11) 9 9876-5432", new EnderecoModel("Rua Tama", "0011A", "Apartamento 304", "Bairro Tama", "12345-011", "Cidade Tama", "Estado Tama")), "Estoquista", "123.45678.91-1", 2500, "Est_Tama")

    public class FuncionarioRepository : IFuncionarioRepository
    {
        private readonly LaughTaleContext _context;
        public FuncionarioRepository()
        {
            _context = new LaughTaleContext();
        }

        public FuncionarioModel BuscarPorId(int id)
        {
            return _context.Funcionarios
                .Include(c => c.Endereco)
                .Where<FuncionarioModel>(c => c.IdFuncionario == id)
                .FirstOrDefault();
        }

        public IEnumerable<FuncionarioModel> BuscarPorNome(string nome)
        {
            return _context.Funcionarios
                .Include(c => c.Endereco)
                .Where<FuncionarioModel>(c => c.NomeCompleto.Contains(nome))
                .ToList();
        }

        public IEnumerable<FuncionarioModel> BuscarTodos()
        {
            return _context.Funcionarios.Include(c => c.Endereco).ToList();
        }

        public void Incluir(FuncionarioModel obj)
        {
            if(ValidarDadosDoFuncionarioService.ValidarDadosDoFuncionario(obj))
            {
                _context.Funcionarios.Add(obj);
                _context.SaveChanges();

            }
            
        }

        public void Excluir(FuncionarioModel model)
        {
            _context.Funcionarios.Remove(model);
            _context.SaveChanges();
        }

        public void Alterar(FuncionarioModel obj)
        {
            if (ValidarDadosDoFuncionarioService.ValidarDadosDoFuncionario(obj))
            {

                _context.Funcionarios.Update(obj);
                _context.SaveChanges();
            }
        }

        
    }
}
