﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Base;
using LaughTale.API.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LaughTale.API.Repository
{
    //new ClienteModel(new PessoaModel("Chimney", "Mãe Chimney", "123456789-06", "(06) 9 9876-5432", new EnderecoModel("Rua Chimney", "006A", "Casa", "Bairro Chimney", "12345-006", "Cidade Chimney", "Estado Chimney"))),
    //new ClienteModel(new PessoaModel("Kokoro", "Mãe Kokoro", "123456789-07", "(07) 9 9876-5432", new EnderecoModel("Rua Kokoro", "007A", "Apartamento 302", "Bairro Kokoro", "12345-007", "Cidade Kokoro", "Estado Kokoro"))),
    //new ClienteModel(new PessoaModel("Charlotte Lola", "Mãe Charlotte", "123456789-08", "(08) 9 9876-5432", new EnderecoModel("Rua Charlotte", "008A", "Casa", "Bairro Charlotte", "12345-008", "Cidade Charlotte", "Estado Charlotte")))

    public class ClienteRepository : IClienteRepository
    {
        private readonly LaughTaleContext _context;
        public ClienteRepository()
        {
            _context = new LaughTaleContext();
        }

        public ClienteModel BuscarPorId(int id)
        {
            return _context.Clientes
                .Include(c => c.Endereco)
                .Where<ClienteModel>(c => c.IdCliente == id)
                .FirstOrDefault();
        }

        public IEnumerable<ClienteModel> BuscarPorNome(string nome)
        {
            return _context.Clientes
                .Include(c => c.Endereco)
                .Where<ClienteModel>(c => c.NomeCompleto.Contains(nome))
                .ToList();
        }

        public IEnumerable<ClienteModel> BuscarTodos()
        {
            return _context.Clientes.Include(c => c.Endereco).ToList();
        }

        public void Incluir(ClienteModel obj)
        {
            _context.Clientes.Add(obj);
            _context.SaveChanges();
        }

        public void Excluir(ClienteModel model)
        {
            _context.Clientes.Remove(model);
            _context.SaveChanges();
        }

        public void Alterar(ClienteModel obj)
        {
            var clienteAlterar = BuscarPorId(obj.IdCliente);
            if (clienteAlterar != null)
            {
                _context.Clientes.Update(obj);
                _context.SaveChanges();
            }
            
        }
    }
}
