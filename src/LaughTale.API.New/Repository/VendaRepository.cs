﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Base;
using LaughTale.API.Repository.Interface;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LaughTale.API.Exceptions;


namespace LaughTale.API.Repository
{
    public class VendaRepository : IVendaRepository
    {
        private readonly LaughTaleContext _context;
        public VendaRepository()
        {
            _context = new LaughTaleContext();
        }

        public VendaModel BuscarPorId(int id)
        {
            return _context.Vendas
                .Where<VendaModel>(c => c.IdVenda == id)
                .FirstOrDefault();
        }

        public IList<VendaModel> BuscarPorNomeCliente(string nomeCliente, string nomeMae)
        {
            ClienteModel cliente = _context.Clientes
                .Where<ClienteModel>(c => c.NomeCompleto.Equals(nomeCliente) && c.NomeDaMae.Equals(nomeMae))
                .FirstOrDefault();
            return _context.Vendas
                .Where<VendaModel>(c => c.IdCliente == cliente.IdCliente)
                .ToList<VendaModel>();
        }

        public IList<VendaModel> BuscarPorNomeFuncionario(string nomeFuncionario)
        {
            FuncionarioModel funcionario = _context.Funcionarios
                .Where<FuncionarioModel>(c => c.NomeCompleto.Contains(nomeFuncionario))
                .FirstOrDefault();
            return _context.Vendas
                .Where<VendaModel>(c => c.IdVendedor == funcionario.IdFuncionario)
                .ToList<VendaModel>();
        }

        public IEnumerable<VendaModel> BuscarTodos()
        {
            return _context.Vendas.ToList();
        }

        public void Incluir(VendaModel obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
        }

        public void Excluir(VendaModel obj)
        {
            _context.Remove(obj);
            _context.SaveChanges();
        }

        public void Alterar(VendaModel obj) => throw new NaoEhPossivelAlterarUmaVendaException("Não é possível alterar uma venda, apenas deleta-la");


    }
}
