﻿using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IQueryRepository<T>
    {
        IEnumerable<T> BuscarTodos();
        T BuscarPorId(int id);
    }
}
