﻿namespace LaughTale.API.Repository.Interface
{
    public interface ICommandRepository<T>
    {
        void Incluir(T obj);
        void Alterar(T obj);
        void Excluir(T obj);
    }
}
