﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IClienteRepository : IQueryRepository<ClienteModel>, ICommandRepository<ClienteModel>
    {
        IEnumerable<ClienteModel> BuscarPorNome(string nome);
    }
}
