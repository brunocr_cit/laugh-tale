﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IFuncionarioRepository : IQueryRepository<FuncionarioModel>, ICommandRepository<FuncionarioModel>
    {
        IEnumerable<FuncionarioModel>  BuscarPorNome(string nome);
    }
}
