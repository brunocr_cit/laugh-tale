﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IVendaRepository : IQueryRepository<VendaModel>, ICommandRepository<VendaModel>
    {
        public IList<VendaModel> BuscarPorNomeCliente(string nomeCliente, string nomeMae);
        public IList<VendaModel> BuscarPorNomeFuncionario(string nomeFuncionario);
    }
}
