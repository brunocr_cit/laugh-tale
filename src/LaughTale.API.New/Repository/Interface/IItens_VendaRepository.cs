﻿using LaughTale.API.Model;

namespace LaughTale.API.Repository.Interface
{
    public interface IItens_VendaRepository : IQueryRepository<ItensVendaModel>, ICommandRepository<ItensVendaModel>
    {
    }
}