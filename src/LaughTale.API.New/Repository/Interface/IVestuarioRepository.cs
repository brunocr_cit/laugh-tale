﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IVestuarioRepository : ICommandRepository<VestuarioModel>, IQueryRepository<VestuarioModel>
    {
        public IEnumerable<VestuarioModel> BuscarPorDescricao(string descricao);

        public void AlterarEstoqueDoVestuario(AlterarEstoqueDoVestuarioModel acrescentarEstoque);
    }
}
