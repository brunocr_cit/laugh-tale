﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Repository.Interface
{
    public interface IFornecedorRepository : IQueryRepository<FornecedorModel>, ICommandRepository<FornecedorModel>
    {
        IEnumerable<FornecedorModel> BuscarPorNome(string nome);
    }
}
