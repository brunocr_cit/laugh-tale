﻿using System;

namespace LaughTale.API.Service
{
    public static class ValidarSalarioFuncionarioService
    {
        public static bool ValidarSalarioFuncionario(double novoSalarioBase)
        {
            if (novoSalarioBase < 0)
            {
                throw new ArgumentException($"O novo salário não pode ser menor que zero, como o apresentado {novoSalarioBase}");
            }
            return true;
        }
    }
}
