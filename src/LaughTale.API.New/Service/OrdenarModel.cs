﻿using System.Linq;
using System.Linq.Dynamic.Core;

namespace LaughTale.API.Service
{
    public static class OrdemExtensions
    {
        public static IQueryable AplicaOrdenacao(this IQueryable query, OrdenarModel ordem)
        {
            if ((ordem != null)&&(!string.IsNullOrEmpty(ordem.OrdenarPor)))
            {
                query = query.OrderBy(ordem.OrdenarPor);
            }
            return query;
        }
    }

    public class OrdenarModel 
    {
        public string OrdenarPor { get; set; }
    }
}
