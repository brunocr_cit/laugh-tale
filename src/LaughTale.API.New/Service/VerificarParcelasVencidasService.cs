﻿using LaughTale.API.Model;
using System;

namespace LaughTale.API.Service
{
    public class VerificarParcelasVencidasService
    {
        public void VerificarVencimentoDaParcela(ParcelaModel parcela)
        {
            if (DateTime.Now > parcela.DataVencimentoParcela)
            {
                parcela.StatusParcelaVencida = true;
            }
        }
    }
}
