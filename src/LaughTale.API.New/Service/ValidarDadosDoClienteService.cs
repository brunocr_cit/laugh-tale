﻿using LaughTale.API.Model;

namespace LaughTale.API.Service
{
    public class ValidarDadosDoClienteService
    {
        public static bool ValidarDadosDoCliente(ClienteModel cliente)
        {
            return ValidarCPFService.ValidarCPF(cliente.CPF) &&
                   ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(cliente.NomeCompleto.Length, 60 ) &&
                   ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(cliente.NomeDaMae.Length, 60) &&
                   ValidarTelefoneService.ValidarTelefone(cliente.TelefoneContato) &&
                   ValidarDadosDoEnderecoService.ValidarDadosDoEndereco(cliente.Endereco);
        }
    }
}
