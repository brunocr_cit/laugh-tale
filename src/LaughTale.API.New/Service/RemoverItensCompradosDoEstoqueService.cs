﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Request;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;

namespace LaughTale.API.Service
{
    public class RemoverItensCompradosDoEstoqueService : IRemoverItensCompradosDoEstoqueService
    {
        private readonly IVestuarioService _vestuarioService;
        private readonly IObjectConverter _objectConverter;
        public RemoverItensCompradosDoEstoqueService(
            IVestuarioService vestuarioService, 
            IObjectConverter objectConverter)
        {
            _vestuarioService = vestuarioService;
            _objectConverter = objectConverter;
        }
        public void RemoverItensCompradosDoEstoque(IList<ItensVendaModel> carroCompras)
        {
            foreach (var item in carroCompras)
            {
                var quantidadeASerAbatidaNoEstoque = new AlterarEstoqueDoVestuarioModel(item.IdItensVenda, item.QuantidadeItens);
                _vestuarioService.AlterarEstoqueDoVestuario(_objectConverter.Map<AlterarEstoqueDoVestuarioRequest>(quantidadeASerAbatidaNoEstoque));
            }
        }
    }
}
