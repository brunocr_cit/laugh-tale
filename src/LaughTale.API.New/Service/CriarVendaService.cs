﻿using LaughTale.API.Extensions;
using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Request;
using LaughTale.API.Service.Interface;
using System;

namespace LaughTale.API.Service
{
    public class CriarVendaService : ICriarVendaService
    {
        private readonly IClienteService _clienteService;
        private readonly IFuncionarioService _funcionarioService;
        private readonly IObjectConverter _objectConverter;
        private readonly IConverterListaItensVendaSimplificadaParaItensVendaService _converterListaItensVenda;
        public CriarVendaService(IClienteService clienteService, 
            IFuncionarioService funcionarioService, 
            IObjectConverter objectConverter,
            
            IConverterListaItensVendaSimplificadaParaItensVendaService converterListaItensVenda
            )
        {
            _clienteService = clienteService;
            _funcionarioService = funcionarioService;
            _objectConverter = objectConverter;
            _converterListaItensVenda = converterListaItensVenda;

        }
        public VendaModel CriarVenda(VendaRequest vendaRequest)
        {
            var venda = new VendaModel();
            venda.CarroCompras = _converterListaItensVenda
                .ConverterListaItensVendaSimplificadaParaItensVenda(vendaRequest.CarroCompras);
            foreach (var item_venda in venda.CarroCompras)
            {
                item_venda.AtualizarAVendaNosItensDoCarroDeCompras(venda);
            }
            venda.DataVenda = DateTime.Now;
            venda.ListaParcelas = CriarParcelaService.CriandoParcelasParaAVenda(vendaRequest.ValorVenda, vendaRequest.QuantidadeParcelas, venda);
            venda.ValorVenda = vendaRequest.ValorVenda;
            //ToDo: Acrescentar dinheiro no caixa
            venda.FormaPagamento = vendaRequest.FormaPagamento.ParaString();
            venda.ComissaoVenda = CalcularComissaoVendaService.CalculandoComissaoVenda(venda.ValorVenda);
            venda.IdCliente = vendaRequest.IdCliente;
            venda.IdVendedor = vendaRequest.IdVendedor;
            
            return venda;
        }

        
    }
}
