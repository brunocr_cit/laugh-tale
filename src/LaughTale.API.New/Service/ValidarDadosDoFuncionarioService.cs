﻿using LaughTale.API.Model;
using System;

namespace LaughTale.API.Service
{
    public static class ValidarDadosDoFuncionarioService
    {
        public static bool ValidarDadosDoFuncionario(FuncionarioModel funcionario)
        {
            return ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(funcionario.NomeCompleto.Length, 60) &&
                ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(funcionario.NomeDaMae.Length, 60) &&
                ValidarCPFService.ValidarCPF(funcionario.CPF) &&
                ValidarTelefoneService.ValidarTelefone(funcionario.TelefoneContato) &&
                ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(funcionario.Cargo.Length, 30) &&
                ValidarPISService.ValidarPIS(funcionario.PIS) &&
                ValidarSalarioFuncionarioService.ValidarSalarioFuncionario(funcionario.Salario) &&
                ValidarSenhaService.ValidarSenha(funcionario.Senha);




        }
    }
}
