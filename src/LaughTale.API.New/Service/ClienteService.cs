﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Request;
using LaughTale.API.Response;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _clienteDao;
        private readonly IObjectConverter _objectConverter;
        public ClienteService(IClienteRepository clienteDao,
                              IObjectConverter objectConverter)
        {
            _clienteDao = clienteDao;
            _objectConverter = objectConverter;
        }
        public void AlterarCliente(ClienteRequest cliente)
        {
             var transformationRequestToModel = _objectConverter.Map<ClienteModel>(cliente);
            _clienteDao.Alterar(transformationRequestToModel);
        }

        public bool DeletarCliente(int idCliente)
        {
            var model = _clienteDao.BuscarPorId(idCliente);
            if (model == null)
            {
                return false;
            }
            _clienteDao.Excluir(model);

            return true;
        }

        public ClienteResponse ObterClientePorId(int idCliente)
        {
            var response = _objectConverter.Map<ClienteResponse>(_clienteDao.BuscarPorId(idCliente));

            return response;
        }

        public List<ClienteResponse> ObterClientePorNome(string NomeCompleto)
        {
            var response = _objectConverter.Map<List<ClienteResponse>>(_clienteDao.BuscarTodos().ToList());

            return response;
        }

        public List<ClienteResponse> ObterListaClientes()
        {
            var response = _objectConverter.Map<List<ClienteResponse>>(_clienteDao.BuscarTodos().ToList());
            return response;
        }

        public ClienteResponse RegistrarCliente(ClienteRequest cliente)
        {
            var transformationRequestToModel = _objectConverter.Map<ClienteModel>(cliente);

            _clienteDao.Incluir(transformationRequestToModel);

            var listaClientes = ObterClientePorNome(transformationRequestToModel.NomeCompleto);

            var clienteIncluido = listaClientes.FirstOrDefault(cliente => cliente.CPF.Equals(transformationRequestToModel.CPF));

            return clienteIncluido;

        }
    }
}
