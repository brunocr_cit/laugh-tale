﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LaughTale.API.Service
{
    public class ValidarCEPService
    {
        public static bool ValidarCEP(string cep)
        {
            Regex Rgx = new Regex("^[0-9]{5}-[0-9]{3}$");

                if (!Rgx.IsMatch(cep)) return false;

                return true;
        }
    }
}
