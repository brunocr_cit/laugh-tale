﻿using LaughTale.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaughTale.API.Service
{
    public class ValidarDadosDoEnderecoService
    {
        public static bool ValidarDadosDoEndereco(EnderecoModel endereco)
        {
            return ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Logradouro.Length, 200) &&
                 ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Numero.Length, 10) &&
                 ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Complemento.Length, 50) &&
                 ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Bairro.Length, 100) &&
                 ValidarCEPService.ValidarCEP(endereco.CEP) &&
                 ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Cidade.Length, 100) &&
                 ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(endereco.Estado.Length, 50);
        }
    }
}
