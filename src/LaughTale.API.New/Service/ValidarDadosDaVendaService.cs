﻿using LaughTale.API.Exceptions;
using LaughTale.API.Extensions;
using LaughTale.API.Model;
using System;


namespace LaughTale.API.Service
{
    public class ValidarDadosDaVendaService
    {
        public bool ValidandoDadosDaVenda(VendaModel venda)
        {
            if (venda.Cliente == null && venda.FormaPagamento == EFormasPagamento.Crediario.ParaString())
            {
                throw new FormaPagamentoInvalidoException("Um cliente não cadastrado não pode realizar o pagamento por meio de parcelas no crediário.");

            }

            foreach (ItensVendaModel carrinho in venda.CarroCompras)
            {
                if (carrinho.QuantidadeItens > carrinho.Mercadoria.EstoqueTotal)
                {
                    throw new QuantidadeExcedenteException($"Tentativa de comprar {carrinho.QuantidadeItens} unidades da mercadoria {carrinho.Mercadoria.Descricao}, porém só temos {carrinho.Mercadoria.EstoqueTotal} unidades no estoque.");
                }
            }

            if (venda.ListaParcelas.Count < 0 || venda.ListaParcelas.Count > 10)
            {
                throw new ArgumentException("A quantidade de parcelas não pode ser menor que 0 e nem maior que 10.");
            }



            return true;
        }
    }
}
