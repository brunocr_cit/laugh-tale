﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Request;
using LaughTale.API.Response;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class FuncionarioService : IFuncionarioService
    {
        private readonly IFuncionarioRepository _funcionarioDao;
        private readonly IObjectConverter _objectConverter;
        public FuncionarioService(IFuncionarioRepository funcionarioDao,
                              IObjectConverter objectConverter)
        {
            _funcionarioDao = funcionarioDao;
            _objectConverter = objectConverter;
        }
        public void AlterarFuncionario(FuncionarioRequest funcionario)
        {
            var transformationRequestToModel = _objectConverter.Map<FuncionarioModel>(funcionario);
            _funcionarioDao.Alterar(transformationRequestToModel);
        }

        public bool DeletarFuncionario(int idFuncionario)
        {
            var model = _funcionarioDao.BuscarPorId(idFuncionario);
            if (model == null)
            {
                return false;
            }
            _funcionarioDao.Excluir(model);

            return true;
        }

        public FuncionarioResponse ObterFuncionarioPorId(int idFuncionario)
        {
            var response = _objectConverter.Map<FuncionarioResponse>(_funcionarioDao.BuscarPorId(idFuncionario));

            return response;
        }

        public List<FuncionarioResponse> ObterFuncionarioPorNome(string NomeCompleto)
        {
            var response = _objectConverter.Map<List<FuncionarioResponse>>(_funcionarioDao.BuscarTodos().ToList());

            return response;
        }

        public List<FuncionarioResponse> ObterListaFuncionarios()
        {
            var response = _objectConverter.Map<List<FuncionarioResponse>>(_funcionarioDao.BuscarTodos().ToList());
            return response;
        }

        public FuncionarioResponse RegistrarFuncionario(FuncionarioRequest funcionario)
        {
            var transformationRequestToModel = _objectConverter.Map<FuncionarioModel>(funcionario);

            _funcionarioDao.Incluir(transformationRequestToModel);

            var listaFuncionarios = ObterFuncionarioPorNome(transformationRequestToModel.NomeCompleto);

            var funcionarioIncluido = listaFuncionarios.FirstOrDefault(funcionario => funcionario.CPF.Equals(transformationRequestToModel.CPF));

            return funcionarioIncluido;

        }

        public void AcrescentarComissaoNoSalarioDoFuncionario(double comissaoVenda, FuncionarioRequest funcionario)
        {
            funcionario.Salario += comissaoVenda;
            AlterarFuncionario(funcionario);
        }
    }
}
