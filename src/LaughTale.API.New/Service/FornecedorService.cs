﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Request;
using LaughTale.API.Response;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class FornecedorService : IFornecedorService
    {
        private readonly IFornecedorRepository _fornecedorDao;
        private readonly IObjectConverter _objectConverter;
        public FornecedorService(IFornecedorRepository fornecedorDao,
                              IObjectConverter objectConverter)
        {
            _fornecedorDao = fornecedorDao;
            _objectConverter = objectConverter;
        }
        public void AlterarFornecedor(FornecedorRequest fornecedor)
        {
            var transformationRequestToModel = _objectConverter.Map<FornecedorModel>(fornecedor);
            _fornecedorDao.Alterar(transformationRequestToModel);
        }

        public bool DeletarFornecedor(int idFornecedor)
        {
            var model = _fornecedorDao.BuscarPorId(idFornecedor);
            if (model == null)
            {
                return false;
            }
            _fornecedorDao.Excluir(model);

            return true;
        }

        public FornecedorResponse ObterFornecedorPorId(int idFornecedor)
        {
            var response = _objectConverter.Map<FornecedorResponse>(_fornecedorDao.BuscarPorId(idFornecedor));

            return response;
        }

        public List<FornecedorResponse> ObterFornecedorPorNome(string NomeCompleto)
        {
            var response = _objectConverter.Map<List<FornecedorResponse>>(_fornecedorDao.BuscarTodos().ToList());

            return response;
        }

        public List<FornecedorResponse> ObterListaFornecedores()
        {
            var response = _objectConverter.Map<List<FornecedorResponse>>(_fornecedorDao.BuscarTodos().ToList());
            return response;
        }

        public FornecedorResponse RegistrarFornecedor(FornecedorRequest fornecedor)
        {
            var transformationRequestToModel = _objectConverter.Map<FornecedorModel>(fornecedor);

            _fornecedorDao.Incluir(transformationRequestToModel);

            var listaFornecedores = ObterFornecedorPorNome(transformationRequestToModel.NomeEmpresa);

            var fornecedorIncluido = listaFornecedores.FirstOrDefault(fornecedor => fornecedor.CNPJ.Equals(transformationRequestToModel.CNPJ));

            return fornecedorIncluido;

        }
    }
}
