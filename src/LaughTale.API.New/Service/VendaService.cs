﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Request;
using LaughTale.API.Response;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository _vendaDao;
        private readonly IObjectConverter _objectConverter;
        private readonly ICriarVendaService _criarVendaService;
        private readonly IClienteService _clienteService;
        private readonly IFuncionarioService _funcionarioService;
        private readonly IRemoverItensCompradosDoEstoqueService _removerItensCompradosEstoque;

        public VendaService(IVendaRepository vendaDao,
                            IObjectConverter objectConverter,
                            ICriarVendaService criarVendaService,
                            IClienteService clienteService,
                            IFuncionarioService funcionarioService,
                            IRemoverItensCompradosDoEstoqueService removerItensCompradosEstoque)
        {
            _vendaDao = vendaDao;
            _objectConverter = objectConverter;
            _criarVendaService = criarVendaService;
            _clienteService = clienteService;
            _funcionarioService = funcionarioService;
            _removerItensCompradosEstoque = removerItensCompradosEstoque;

        }
        public void AlterarVenda(VendaRequest venda)
        {
            var transformationRequestToModel = _objectConverter.Map<VendaModel>(venda);
            _vendaDao.Alterar(transformationRequestToModel);
            
        }

        public bool DeletarVenda(int idVenda)
        {
            var model = _vendaDao.BuscarPorId(idVenda);
            if (model == null)
            {
                return false;
            }
            _vendaDao.Excluir(model);

            return true;
        }

        public VendaResponse ObterVendaPorId(int idVenda)
        {
            var response = _objectConverter.Map<VendaResponse>(_vendaDao.BuscarPorId(idVenda));

            return response;
        }

        public IList<VendaResponse> ObterVendaPorNomeCliente(string nomeCompleto, string nomeMae)
        {
            var response = _objectConverter.Map<IList<VendaResponse>>(_vendaDao.BuscarPorNomeCliente(nomeCompleto, nomeMae));
            return response;
        }

        public IList<VendaResponse> ObterVendaPorNomeFuncionario(string NomeCompleto)
        {
            var response = _objectConverter.Map<IList<VendaResponse>>(_vendaDao.BuscarPorNomeFuncionario(NomeCompleto));
            return response;
        }

        public IList<VendaResponse> ObterListaVendas()
        {
            var response = _objectConverter.Map<List<VendaResponse>>(_vendaDao.BuscarTodos().ToList());
            return response;
        }

        public VendaResponse RegistrarVenda(VendaRequest venda)
        {

            var vendaModel = _criarVendaService.CriarVenda(venda);

            if (vendaModel != null)
            {
                _vendaDao.Incluir(vendaModel);

                _funcionarioService.AcrescentarComissaoNoSalarioDoFuncionario(vendaModel.ComissaoVenda, _objectConverter.Map<FuncionarioRequest>(vendaModel.Funcionario));
                _removerItensCompradosEstoque.RemoverItensCompradosDoEstoque(vendaModel.CarroCompras);
                return _objectConverter.Map<VendaResponse>(vendaModel);
            }

            return null;

        }

        
    }
}
