﻿using LaughTale.API.Extensions;
using LaughTale.API.Exceptions;
using LaughTale.API.Model;

namespace LaughTale.API.Service
{
    public static class ValidarDadosVestuarioService
    {
        public static VestuarioModel ValidarDadosVestuario(VestuarioModel vestuario)
        {
            if (vestuario.Funcionario.Cargo != ECargo.Estoquista.ParaString())
            {
                throw new PermissaoNegadaException($"O funcionario não tem permissão para cadastrar mercadorias.");
            }
            vestuario.EstoqueTotal = vestuario.Quantidade;

            return vestuario;
        }
    }
}
