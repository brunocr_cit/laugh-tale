﻿using LaughTale.API.Model;
using System;
using System.Collections.Generic;

namespace LaughTale.API.Service
{
    public class CriarParcelaService
    {
        public static IList<ParcelaModel> CriandoParcelasParaAVenda(double valorVenda, int numeroParcelas, VendaModel venda)
        {
            List<ParcelaModel> listaDeParcelas = new List<ParcelaModel>();
            double valorDaParcela;
            if (numeroParcelas > 0)
            {
                valorDaParcela = valorVenda / numeroParcelas;

                for (int i = 1; i <= numeroParcelas; i++)
                {
                    ParcelaModel parcela = new ParcelaModel();
                    parcela.ValorParcela = valorDaParcela;
                    parcela.DataVencimentoParcela = DateTime.Now.AddMonths(i);
                    parcela.Venda = venda;
                    listaDeParcelas.Add(parcela);
                }
            }
            else 
            {
                valorDaParcela = valorVenda;
                ParcelaModel parcela = new ParcelaModel();
                parcela.ValorParcela = valorDaParcela;
                parcela.DataVencimentoParcela = DateTime.Now;
                parcela.Venda = venda;
                listaDeParcelas.Add(parcela);
            }
            

            return listaDeParcelas;
        }
    }
}
