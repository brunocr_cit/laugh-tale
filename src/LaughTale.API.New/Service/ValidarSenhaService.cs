﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LaughTale.API.Service
{
    public class ValidarSenhaService
    {
        public static bool ValidarSenha(string senha)
        { 
            string verificador = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,20})";
            return Regex.IsMatch(senha, verificador);
        }
    }
}
