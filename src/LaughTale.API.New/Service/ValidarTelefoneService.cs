﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LaughTale.API.Service
{
    public class ValidarTelefoneService
    {
        public static bool ValidarTelefone(string telefone)
        {
            string espressaoValidacao = "^((?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])) (?:[2-8]|9[1-9])[0-9]{3}-[0-9]{4}$";
            return Regex.IsMatch(telefone, espressaoValidacao);
        }
    }
}
