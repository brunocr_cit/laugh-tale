﻿using LaughTale.API.Model;

namespace LaughTale.API.Service
{
    public class ValidarDadosDoFornecedorService
    {
        public static bool ValidarDadosDoFornecedor(FornecedorModel fornecedor)
        {
            return ValidarQuantidadeDeCaracteresService.ValidarQuantidadeDeCaracteres(fornecedor.NomeEmpresa.Length, 100) &&
                ValidarCNPJService.ValidarCNPJ(fornecedor.CNPJ) &&
                ValidarEmailService.ValidarEmail(fornecedor.EmailContato) &&
                ValidarTelefoneService.ValidarTelefone(fornecedor.TelefoneContato) &&
                ValidarDadosDoEnderecoService.ValidarDadosDoEndereco(fornecedor.Endereco);
        }
    }
}
