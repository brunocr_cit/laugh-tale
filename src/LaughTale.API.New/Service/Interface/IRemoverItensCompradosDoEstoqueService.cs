﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IRemoverItensCompradosDoEstoqueService
    {
        public void RemoverItensCompradosDoEstoque(IList<ItensVendaModel> carroCompras);
    }
}
