﻿using LaughTale.API.Model;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IConverterListaItensVendaSimplificadaParaItensVendaService
    {
        public IList<ItensVendaModel> ConverterListaItensVendaSimplificadaParaItensVenda(IList<ItensVendaSimplificadoModel> carroComprasSimplificado);
    }
}
