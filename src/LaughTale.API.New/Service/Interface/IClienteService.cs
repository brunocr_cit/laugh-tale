﻿using LaughTale.API.Request;
using LaughTale.API.Response;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IClienteService
    {
        public List<ClienteResponse> ObterListaClientes();

        public ClienteResponse ObterClientePorId(int idCliente);

        public List<ClienteResponse> ObterClientePorNome(string NomeCompleto);

        public ClienteResponse RegistrarCliente(ClienteRequest cliente);

        public void AlterarCliente(ClienteRequest cliente);

        public bool DeletarCliente(int idCliente);
    }
}
