﻿using LaughTale.API.Request;
using LaughTale.API.Response;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IVestuarioService
    {
        public List<VestuarioResponse> ObterListaVestuarios();

        public VestuarioResponse ObterVestuarioPorId(int idVestuario);

        public List<VestuarioResponse> ObterVestuarioPorDescricao(string descricao);

        public VestuarioResponse RegistrarVestuario(VestuarioRequest vestuario);

        public void AlterarVestuario(VestuarioRequest vestuario);

        public void AlterarEstoqueDoVestuario(AlterarEstoqueDoVestuarioRequest acrescentarEstoque);

        public bool DeletarVestuario(int idMercadoria);
    }
}
