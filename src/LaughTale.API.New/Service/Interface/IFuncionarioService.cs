﻿using LaughTale.API.Request;
using LaughTale.API.Response;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IFuncionarioService
    {
        public List<FuncionarioResponse> ObterListaFuncionarios();

        public FuncionarioResponse ObterFuncionarioPorId(int idFuncionario);

        public List<FuncionarioResponse> ObterFuncionarioPorNome(string NomeCompleto);

        public FuncionarioResponse RegistrarFuncionario(FuncionarioRequest funcionario);

        public void AlterarFuncionario(FuncionarioRequest funcionario);

        public bool DeletarFuncionario(int idFuncionario);
        public void AcrescentarComissaoNoSalarioDoFuncionario(double comissaoVenda, FuncionarioRequest funcionario);
    }
}
