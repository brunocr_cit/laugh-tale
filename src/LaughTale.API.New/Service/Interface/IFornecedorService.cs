﻿using LaughTale.API.Request;
using LaughTale.API.Response;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IFornecedorService
    {
        public List<FornecedorResponse> ObterListaFornecedores();

        public FornecedorResponse ObterFornecedorPorId(int idFornecedor);

        public List<FornecedorResponse> ObterFornecedorPorNome(string NomeCompleto);

        public FornecedorResponse RegistrarFornecedor(FornecedorRequest fornecedor);

        public void AlterarFornecedor(FornecedorRequest fornecedor);

        public bool DeletarFornecedor(int idFornecedor);
    }
}
