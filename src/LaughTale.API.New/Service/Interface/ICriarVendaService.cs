﻿using LaughTale.API.Model;
using LaughTale.API.Request;

namespace LaughTale.API.Service.Interface
{
    public interface ICriarVendaService
    {
        public VendaModel CriarVenda(VendaRequest vendaRequest);
    }
}
