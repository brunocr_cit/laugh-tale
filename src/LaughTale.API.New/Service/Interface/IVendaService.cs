﻿using LaughTale.API.Request;
using LaughTale.API.Response;
using System.Collections.Generic;

namespace LaughTale.API.Service.Interface
{
    public interface IVendaService
    {
        public IList<VendaResponse> ObterListaVendas();

        public VendaResponse ObterVendaPorId(int idVenda);

        public IList<VendaResponse> ObterVendaPorNomeCliente(string nomeCompleto, string nomeMae);

        public IList<VendaResponse> ObterVendaPorNomeFuncionario(string nomeCompleto);

        public VendaResponse RegistrarVenda(VendaRequest venda);

        public void AlterarVenda(VendaRequest venda);

        public bool DeletarVenda(int idVenda);
    }
}
