﻿using LaughTale.API.Mapper;
using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Request;
using LaughTale.API.Response;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class VestuarioService : IVestuarioService
    {
        private readonly IVestuarioRepository _vestuarioDao;
        private readonly IObjectConverter _objectConverter;
        public VestuarioService(IVestuarioRepository vestuarioDao,
                              IObjectConverter objectConverter)
        {
            _vestuarioDao = vestuarioDao;
            _objectConverter = objectConverter;
        }
        public void AlterarVestuario(VestuarioRequest vestuario)
        {
            var transformationRequestToModel = _objectConverter.Map<VestuarioModel>(vestuario);
            _vestuarioDao.Alterar(transformationRequestToModel);
        }

        public void AlterarEstoqueDoVestuario(AlterarEstoqueDoVestuarioRequest acrescentarEstoque)
        { 
            var transformationRequestToModel = _objectConverter.Map<AlterarEstoqueDoVestuarioModel>(acrescentarEstoque);
            _vestuarioDao.AlterarEstoqueDoVestuario(transformationRequestToModel);
        }

        public bool DeletarVestuario(int idVestuario)
        {
            var model = _vestuarioDao.BuscarPorId(idVestuario);
            if (model == null)
            {
                return false;
            }
            _vestuarioDao.Excluir(model);

            return true;
        }

        public VestuarioResponse ObterVestuarioPorId(int idVestuario)
        {
            var response = _objectConverter.Map<VestuarioResponse>(_vestuarioDao.BuscarPorId(idVestuario));

            return response;
        }

        public List<VestuarioResponse> ObterVestuarioPorDescricao(string descricao)
        {
            var response = _objectConverter.Map<List<VestuarioResponse>>(_vestuarioDao.BuscarTodos().ToList());

            return response;
        }

        public List<VestuarioResponse> ObterListaVestuarios()
        {
            var response = _objectConverter.Map<List<VestuarioResponse>>(_vestuarioDao.BuscarTodos().ToList());
            return response;
        }

        public VestuarioResponse RegistrarVestuario(VestuarioRequest vestuario)
        {
            var transformationRequestToModel = _objectConverter.Map<VestuarioModel>(vestuario);

            _vestuarioDao.Incluir(transformationRequestToModel);

            return _objectConverter.Map<VestuarioResponse>(transformationRequestToModel);

        }
    }
}
