﻿using LaughTale.API.Model;
using LaughTale.API.Repository.Interface;
using LaughTale.API.Service.Interface;
using System.Collections.Generic;
using System.Linq;

namespace LaughTale.API.Service
{
    public class ConverterListaItensVendaSimplificadaParaItensVendaService : IConverterListaItensVendaSimplificadaParaItensVendaService
    {
        private readonly IVestuarioRepository _mercadoriaDao;
        public ConverterListaItensVendaSimplificadaParaItensVendaService(IVestuarioRepository mercadoriaDao)
        {
            _mercadoriaDao = mercadoriaDao;
        }
        public IList<ItensVendaModel> ConverterListaItensVendaSimplificadaParaItensVenda(IList<ItensVendaSimplificadoModel> carroComprasSimplificado)
        {
            
            IList<ItensVendaModel> carroCompras = new List<ItensVendaModel>();
            //ToDo: Fazer a busca na lista resultando utilizando o FirstOrDefault
            List<VestuarioModel> listaVestuarios = _mercadoriaDao.BuscarTodos().ToList();

            foreach (var item in carroComprasSimplificado)
            {
                var vestuario = listaVestuarios.FirstOrDefault(v => v.IdVestuario == item.IdMercadoria);
                ItensVendaModel item_venda = new ItensVendaModel();
                item_venda.IdVestuario = item.IdMercadoria;
                item_venda.QuantidadeItens = item.QuantidadeItens;
                item_venda.Mercadoria = vestuario;
                carroCompras.Add(item_venda);
            }

            return carroCompras;
        }
    }
}
