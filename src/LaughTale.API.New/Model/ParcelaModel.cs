﻿using System;

namespace LaughTale.API.Model
{
    public class ParcelaModel
    {
        public int IdParcela { get; set; }
        public double ValorParcela { get; set; }
        public bool StatusParcelaPaga { get; set; }
        public DateTime DataVencimentoParcela { get; set; }
        public bool StatusParcelaVencida { get; set; }
        public int IdVenda { get; set; }
        public VendaModel Venda { get; set; }

    }
}
