﻿using System.Collections.Generic;

namespace LaughTale.API.Model
{
    public class ClienteModel : PessoaModel
    {
        public int IdCliente { get; set; }
        public IEnumerable<VendaModel> ComprasRealizadas { get; set; }


    }
}
