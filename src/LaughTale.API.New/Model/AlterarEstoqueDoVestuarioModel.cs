﻿namespace LaughTale.API.Model
{
    public class AlterarEstoqueDoVestuarioModel
    {
        public int IdMercadoria { get; set; }
        public int QuantidadeASerAlterada{ get; set; }
        public AlterarEstoqueDoVestuarioModel(int idMercadoria, int quantidadeASerAlterada)
        {
            IdMercadoria = idMercadoria;
            QuantidadeASerAlterada = quantidadeASerAlterada;
        }
    }
}
