﻿namespace LaughTale.API.Model
{
    public class EnderecoModel
    {
        public int IdEndereco { get; set; }
        public string Logradouro { get; private set; }
        public string Numero { get; private set; }
        public string Complemento { get; set; }
        public string Bairro { get; private set; }
        public string CEP { get; set; }
        public string Cidade { get; private set; }
        public string Estado { get; private set; }
        public ClienteModel Cliente { get; set; }
        public FuncionarioModel Funcionario { get; set; }
        public FornecedorModel Fornecedor { get; set; }



    }
}
