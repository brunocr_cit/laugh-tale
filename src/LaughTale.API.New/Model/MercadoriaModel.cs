﻿namespace LaughTale.API.Model
{
    public class MercadoriaModel
    {
        public string CodigoBarras { get; set; }
        public int EstoqueTotal { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public double PrecoUnidadeFornecedor { get; set; }
        public double PrecoUnidadeCliente { get; set; }
        public FornecedorModel Fornecedor { get; set; }
        public int IdFornecedor { get; set; }
        public FuncionarioModel Funcionario { get; set; }
        public int IdEstoquista { get; set; }
        public ItensVendaModel ItensVenda { get; set; }

        public void ModificarPreco(double valorModificado)
        {
            PrecoUnidadeCliente = valorModificado;
        }
    }
}
