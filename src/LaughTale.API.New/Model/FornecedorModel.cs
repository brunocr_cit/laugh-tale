﻿namespace LaughTale.API.Model
{
    public class FornecedorModel
    {
        public int IdFornecedor { get; set; }
        public string NomeEmpresa { get; set; }
        public string CNPJ { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public EnderecoModel Endereco { get; set; }
        public int IdEndereco { get; set; }
        public VestuarioModel Vestuario { get; set; }

        public FornecedorModel()
        {
            Endereco = new EnderecoModel();
        }

    }
}
