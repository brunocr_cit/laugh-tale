﻿using System;
using System.Collections.Generic;

namespace LaughTale.API.Model
{
    public class VendaModel 
    {
        public int IdVenda { get; set; }
        public IList<ItensVendaModel> CarroCompras { get; set; }
        public DateTime DataVenda { get; set; }
        public string FormaPagamento { get; set; }
        public IList<ParcelaModel> ListaParcelas { get; set; }
        public double ValorVenda { get; set; }
        public double ComissaoVenda { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
        public ClienteModel Cliente { get; set; }
        public FuncionarioModel Funcionario { get; set; }

        public VendaModel()
        {
           
        }

        public void DetalhesVenda()
        {
            //Console.WriteLine($"O cliente {Cliente.Pessoa.NomeCompleto} está comprando: ");

            //foreach (var produtos in CarroCompras)
            //{
            //    Console.WriteLine($"{produtos.QuantidadeMercadoria} peças do produto {produtos.Mercadoria.Descricao}");
            //    Console.WriteLine($"Restam no estoque {produtos.Mercadoria.Quantidade} unidades.");
            //}

            //Console.WriteLine($"O vendedor {Vendedor.Pessoa.NomeCompleto} adquiriu uma comissão de R${ComissaoVenda}," +
            //    $" totalizando um salário de R${Vendedor.Salario}");
            //Console.WriteLine($"Dinheiro em caixa R$ {CaixaRegistradoraModel.QuantidadeDinheiro}");
            //Console.WriteLine($"Até o momento já foram executadas {QuantidadeVendas} vendas");

        }
    }
}
