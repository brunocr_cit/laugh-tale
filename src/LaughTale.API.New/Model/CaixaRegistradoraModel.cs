﻿namespace LaughTale.API.Model
{
    public class CaixaRegistradoraModel
    {
        public int IdCaixaRegistradora { get; set; }
        public double QuantidadeDinheiroArmazenado { get; set; }

    }
}
