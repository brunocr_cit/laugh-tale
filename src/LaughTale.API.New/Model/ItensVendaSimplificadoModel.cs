﻿namespace LaughTale.API.Model
{
    public class ItensVendaSimplificadoModel
    {
        public int QuantidadeItens { get; set; }
        public int IdMercadoria { get; set; }
    }
}
