﻿namespace LaughTale.API.Model
{
    public class ItensVendaModel
    {
        public int IdItensVenda { get; set; }
        public int QuantidadeItens { get; set; }
        public int IdVestuario { get; set; }
        public VestuarioModel Mercadoria { get; set; }
        public int IdVenda { get; set; }
        public VendaModel Venda { get; set; }
        
        public void AtualizarAVendaNosItensDoCarroDeCompras(VendaModel venda)
        {
            IdVenda = venda.IdVenda;

        }
    }
}
