﻿using System.Collections.Generic;

namespace LaughTale.API.Model
{
    public class FuncionarioModel : PessoaModel
    {
        public int IdFuncionario { get; private set; }
        public string Cargo { get; set; }
        public string PIS { get; private set; }
        public double Salario { get; private set; } 
        public string Senha { get; set; }
        public VestuarioModel Vestuario { get; set; }
        public IEnumerable<VendaModel> VendasRealizadas { get; set; }

        
        
    }
}
