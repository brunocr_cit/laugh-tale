﻿namespace LaughTale.API.Model
{
    public class PessoaModel
    {
        public string NomeCompleto { get; private set; }
        public string NomeDaMae { get; private set; }
        public string CPF { get; private set; }
        public string TelefoneContato { get; set; }
        public EnderecoModel Endereco { get; set; }
        public int IdEndereco { get; set; }

        public PessoaModel()
        {
            Endereco = new EnderecoModel();
        }
    }
}
