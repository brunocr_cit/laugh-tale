﻿namespace LaughTale.API.Model
{
    public class VestuarioModel : MercadoriaModel
    {
        public int IdVestuario { get; set; }
        public string Tamanho { get; set; }
        
    }
}
