﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;

namespace LaughTale.DAL.Clientes
{
    public interface IClienteRepository : IQuery<ClienteModel>, ICommand<ClienteModel>
    {
        ClienteModel BuscarPorNome(string nome);
    }
}
