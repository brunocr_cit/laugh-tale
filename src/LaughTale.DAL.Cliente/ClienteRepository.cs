﻿using LaughTale.Extensions;
using LaughTale.Modelos;
using System.Collections.Generic;

namespace LaughTale.DAL.Clientes
{
    public class ClienteRepository : IClienteRepository
    {
        private static List<ClienteModel> Clientes_DB { get; set; }

        public ClienteRepository()
        {
            Clientes_DB.AdicionarVarios(
                new ClienteModel(new PessoaModel("Chimney", "Mãe Chimney", "123456789-06", "(06) 9 9876-5432", new EnderecoModel("Rua Chimney", "006A", "Casa", "Bairro Chimney", "12345-006", "Cidade Chimney", "Estado Chimney"))),
                new ClienteModel(new PessoaModel("Kokoro", "Mãe Kokoro", "123456789-07", "(07) 9 9876-5432", new EnderecoModel("Rua Kokoro", "007A", "Apartamento 302", "Bairro Kokoro", "12345-007", "Cidade Kokoro", "Estado Kokoro"))),
                new ClienteModel(new PessoaModel("Charlotte Lola", "Mãe Charlotte", "123456789-08", "(08) 9 9876-5432", new EnderecoModel("Rua Charlotte", "008A", "Casa", "Bairro Charlotte", "12345-008", "Cidade Charlotte", "Estado Charlotte")))
                );
        }

        public ClienteModel BuscarPorId(int id)
        {
            foreach (ClienteModel cliente in Clientes_DB)
            {
                if (cliente.IdCliente == id)
                {
                    return cliente;
                }
            }
            return null;
        }

        public ClienteModel BuscarPorNome(string nome)
        {
            foreach (ClienteModel cliente in Clientes_DB)
            {
                if (cliente.Pessoa.NomeCompleto == nome)
                {
                    return cliente;
                }
            }
            return null;
        }

        public IEnumerable<ClienteModel> BuscarTodos()
        {
            return Clientes_DB;
        }

        public void Incluir(ClienteModel obj)
        {
            Clientes_DB.Add(obj);
        }

        public void Excluir(ClienteModel obj)
        {
            Clientes_DB.Remove(obj);
        }

        public void Alterar(ClienteModel obj)
        {
            foreach (ClienteModel cliente in Clientes_DB)
            {
                if (cliente.IdCliente == obj.IdCliente)
                {
                    Clientes_DB.Remove(cliente);
                    Clientes_DB.Add(obj);
                }
            }

        }
    }
}
