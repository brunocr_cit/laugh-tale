﻿using LaughTale.DAL.CRUD;
using LaughTale.Modelos;

namespace LaughTale.DAL.Vestuarios
{
    public interface IVestuarioRepository : ICommandRepository<VestuarioModel>, IQueryRepository<VestuarioModel>
    {
    }
}
