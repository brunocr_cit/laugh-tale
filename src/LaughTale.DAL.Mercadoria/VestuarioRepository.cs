﻿using LaughTale.DAL.Fornecedores;
using LaughTale.DAL.Funcionarios;
using LaughTale.Extensions;
using LaughTale.Modelos;
using System.Collections.Generic;


namespace LaughTale.DAL.Vestuarios
{
    public class VestuarioRepository : IVestuarioRepository
    {
        private static List<VestuarioModel> Vestuarios_DB { get; set; }

        public VestuarioRepository()
        {
            FuncionarioRepository funcionarioDao = new FuncionarioRepository();
            FornecedorRepository fornecedorDao = new FornecedorRepository();
            Vestuarios_DB.AdicionarVarios(
                new VestuarioModel(100001, "Regatas masculinas de malha estampadas P", 10, 25, 50, fornecedorDao.BuscarPorNome("Empresa Kureha"), funcionarioDao.BuscarPorNome("Tama"), "P"),
                new VestuarioModel(100002, "Regatas masculinas de malha estampadas M", 10, 27.50, 55, fornecedorDao.BuscarPorNome("Empresa Kureha"), funcionarioDao.BuscarPorNome("Tama"), "M"),
                new VestuarioModel(100003, "Regatas masculinas de malha estampadas G", 10, 27.50, 55, fornecedorDao.BuscarPorNome("Empresa Kureha"), funcionarioDao.BuscarPorNome("Tama"), "G"),
                new VestuarioModel(100004, "Regatas masculinas de malha estampadas GG", 10, 27.50, 55, fornecedorDao.BuscarPorNome("Empresa Kureha"), funcionarioDao.BuscarPorNome("Tama"), "GG"),
                new VestuarioModel(100005, "Calças jeans com detalhes em rasgado 38", 10, 42.50, 85, fornecedorDao.BuscarPorNome("Empresa Vinsmoke Reiju"), funcionarioDao.BuscarPorNome("Tama"), "38"),
                new VestuarioModel(100006, "Calças jeans com detalhes em rasgado 40", 10, 45, 90, fornecedorDao.BuscarPorNome("Empresa Vinsmoke Reiju"), funcionarioDao.BuscarPorNome("Tama"), "40")
                ); 
        }

        public VestuarioModel BuscarPorId(int id)
        {
            foreach (VestuarioModel mercadoria in Vestuarios_DB)
            {
                if(mercadoria.IdMercadoria == id)
                {
                    return mercadoria;
                }
            }
            return null;
        }

        public IEnumerable<VestuarioModel> BuscarTodos()
        {
            return Vestuarios_DB;
        }

        public void Incluir(VestuarioModel obj)
        {
            Vestuarios_DB.Add(obj);
        }

        public void Excluir(VestuarioModel obj)
        {
            Vestuarios_DB.Remove(obj);
        }

        public void Alterar(VestuarioModel obj)
        {
            foreach (VestuarioModel mercadoria in Vestuarios_DB)
            {
                if (mercadoria.IdMercadoria == obj.IdMercadoria)
                {
                    Vestuarios_DB.Remove(mercadoria);
                    Vestuarios_DB.Add(obj);
                }
            }

        }
    }
}
