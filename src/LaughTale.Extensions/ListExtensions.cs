﻿using System;
using System.Collections.Generic;

namespace LaughTale.Extensions
{
    public static class ListExtentions
    {
        public static void AdicionarVarios<T>(this List<T> lista, params T[] itens)
        {
            foreach (T item in itens)
            {
                lista.Add(item);
            }
        }
    }
}
