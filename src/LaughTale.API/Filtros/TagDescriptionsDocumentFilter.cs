﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaughTale.API.Filtros
{
    public class TagDescriptionsDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new[] {
            new Tag { Name = "Cliente", Description = "Consulta e mantém os clientes." },
            new Tag { Name = "Fornecedor", Description = "Consulta e mantém os fornecedores." },
            new Tag { Name = "Funcionario", Description = "Consulta e mantém os funcionarios." },
            new Tag { Name = "Mercadoria", Description = "Consulta e mantém as mercadorias." },
            new Tag { Name = "Venda", Description = "Consulta e mantém as vendas." },
            new Tag { Name = "Item_Venda", Description = "Consulta e mantém os itens que compõem o carrinho de uma venda." } 
        };
        }
    }
}
