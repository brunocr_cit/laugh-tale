﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Swashbuckle.AspNetCore.Annotations;
using LaughTale.API.Model.Erros;
using LaughTale.API.Model.Paginacao;
using LaughTale.API.Model.Ordenacao;
using LaughTale.Repository;
using LaughTale.API.Model;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ClienteController : ControllerBase
    {
        private readonly ClienteRepository _clienteDao;

        public ClienteController(ClienteRepository clienteDao)
        {
            _clienteDao = clienteDao;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de clientes.",
            Tags = new[] { "Cliente" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(Paginado<ClienteModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult ListaClientes(
            [FromQuery] Ordem ordem,
            [FromQuery] Paginacao paginacao)
        {

            var lista = _clienteDao.BuscarTodos().ToList();
            var listaPaginada = Paginado<ClienteModel>.From(paginacao, lista);

            if (listaPaginada.Resultado.Count == 0)
            {
                return NotFound();
            }

            return Ok(listaPaginada);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "Recupera o cliente identificado por seu {id}.",
            Tags = new[] { "Cliente" },
            Produces = new[] { "application/json", "application/xml" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(ClienteModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorId(
            [SwaggerParameter("Id do cliente.", Required = true)] int id)
        {
            var model = _clienteDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpGet("{nome}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(ClienteModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorNome(
            [SwaggerParameter("Nome do cliente.", Required = true)] string nome)
        {
            var model = _clienteDao.BuscarPorNome(nome);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo cliente na base.",
            Tags = new[] { "Cliente" }
            )]
        [ProducesResponseType(statusCode: 201, Type = typeof(ClienteModel))]
        [ProducesResponseType(statusCode: 400, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        public IActionResult Incluir([FromForm] ClienteModel model)
        {
            if (ModelState.IsValid)
            {
                _clienteDao.Incluir(model);
                var uri = Url.Action("RecuperarPorId", new { id = model.IdCliente });
                return Created(uri, model);
            }
            return BadRequest(ErroResponse.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o cliente na base.",
            Tags = new[] { "Cliente" })]
        [ProducesResponseType(statusCode: 200)]
        [ProducesResponseType(400, Type = typeof(ErroResponse))]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Alterar([FromForm] ClienteModel model)
        {
            if (ModelState.IsValid)
            {
                _clienteDao.Alterar(model);
                return Ok(); 
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o cliente da base.",
            Tags = new[] { "Cliente" }
        )]
        [ProducesResponseType(statusCode: 204)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Remover(int id)
        {
            var model = _clienteDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            _clienteDao.Excluir(model);
            return NoContent();
        }
    }
}
