﻿using LaughTale.API.Model.Erros;
using LaughTale.API.Model.Ordenacao;
using LaughTale.API.Model.Paginacao;
using LaughTale.DAL.Vendas;
using LaughTale.Modelos;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaRepository _vendaDao;

        public VendaController(VendaRepository vendaDao)
        {
            _vendaDao = vendaDao;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de vendas.",
            Tags = new[] { "Venda" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(Paginado<VendaModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult ListaVendas(
            [FromQuery] Ordem ordem,
            [FromQuery] Paginacao paginacao)
        {

            var lista = _vendaDao.BuscarTodos().ToList();
            var listaPaginada = Paginado<VendaModel>.From(paginacao, lista);

            if (listaPaginada.Resultado.Count == 0)
            {
                return NotFound();
            }

            return Ok(listaPaginada);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "Recupera o venda identificado por seu {id}.",
            Tags = new[] { "Venda" },
            Produces = new[] { "application/json", "application/xml" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(VendaModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorId(
            [SwaggerParameter("Id do venda.", Required = true)] int id)
        {
            var model = _vendaDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo venda na base.",
            Tags = new[] { "Venda" }
            )]
        [ProducesResponseType(statusCode: 201, Type = typeof(VendaModel))]
        [ProducesResponseType(statusCode: 400, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        public IActionResult Incluir([FromForm] VendaModel model)
        {
            if (ModelState.IsValid)
            {
                _vendaDao.Incluir(model);
                var uri = Url.Action("RecuperarPorId", new { id = model.IdVenda });
                return Created(uri, model);
            }
            return BadRequest(ErroResponse.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o venda na base.",
            Tags = new[] { "Venda" })]
        [ProducesResponseType(statusCode: 200)]
        [ProducesResponseType(400, Type = typeof(ErroResponse))]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Alterar([FromForm] VendaModel model)
        {
            if (ModelState.IsValid)
            {
                _vendaDao.Alterar(model);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o venda da base.",
            Tags = new[] { "Venda" }
        )]
        [ProducesResponseType(statusCode: 204)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Remover(int id)
        {
            var model = _vendaDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            _vendaDao.Excluir(model);
            return NoContent();
        }
    }
}
