﻿using LaughTale.API.Model.Erros;
using LaughTale.API.Model.Ordenacao;
using LaughTale.API.Model.Paginacao;
using LaughTale.DAL.Fornecedores;
using LaughTale.Modelos;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class FornecedorController : ControllerBase
    {
        private readonly FornecedorRepository _fornecedorDao;

        public FornecedorController(FornecedorRepository fornecedorDao)
        {
            _fornecedorDao = fornecedorDao;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de fornecedors.",
            Tags = new[] { "Fornecedor" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(Paginado<FornecedorModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult ListaFornecedores(
            [FromQuery] Ordem ordem,
            [FromQuery] Paginacao paginacao)
        {

            var lista = _fornecedorDao.BuscarTodos().ToList();
            var listaPaginada = Paginado<FornecedorModel>.From(paginacao, lista);

            if (listaPaginada.Resultado.Count == 0)
            {
                return NotFound();
            }

            return Ok(listaPaginada);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "Recupera o fornecedor identificado por seu {id}.",
            Tags = new[] { "Fornecedor" },
            Produces = new[] { "application/json", "application/xml" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(FornecedorModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorId(
            [SwaggerParameter("Id do fornecedor.", Required = true)] int id)
        {
            var model = _fornecedorDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpGet("{nome}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(FornecedorModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorNome(
            [SwaggerParameter("Nome do fornecedor.", Required = true)] string nome)
        {
            var model = _fornecedorDao.BuscarPorNome(nome);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo fornecedor na base.",
            Tags = new[] { "Fornecedor" }
            )]
        [ProducesResponseType(statusCode: 201, Type = typeof(FornecedorModel))]
        [ProducesResponseType(statusCode: 400, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        public IActionResult Incluir([FromForm] FornecedorModel model)
        {
            if (ModelState.IsValid)
            {
                _fornecedorDao.Incluir(model);
                var uri = Url.Action("RecuperarPorId", new { id = model.IdFornecedor });
                return Created(uri, model);
            }
            return BadRequest(ErroResponse.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o fornecedor na base.",
            Tags = new[] { "Fornecedor" })]
        [ProducesResponseType(statusCode: 200)]
        [ProducesResponseType(400, Type = typeof(ErroResponse))]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Alterar([FromForm] FornecedorModel model)
        {
            if (ModelState.IsValid)
            {
                _fornecedorDao.Alterar(model);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o fornecedor da base.",
            Tags = new[] { "Fornecedor" }
        )]
        [ProducesResponseType(statusCode: 204)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Remover(int id)
        {
            var model = _fornecedorDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            _fornecedorDao.Excluir(model);
            return NoContent();
        }
    }
}
