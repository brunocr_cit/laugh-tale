﻿using LaughTale.API.Model.Erros;
using LaughTale.API.Model.Ordenacao;
using LaughTale.API.Model.Paginacao;
using LaughTale.DAL.Vestuarios;
using LaughTale.Modelos;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class VestuarioController : ControllerBase
    {
        private readonly VestuarioRepository _mercadoriaDao;

        public VestuarioController(VestuarioRepository mercadoriaDao)
        {
            _mercadoriaDao = mercadoriaDao;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de mercadorias.",
            Tags = new[] { "Vestuario" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(Paginado<VestuarioModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult ListaVestuarios(
            [FromQuery] Ordem ordem,
            [FromQuery] Paginacao paginacao)
        {

            var lista = _mercadoriaDao.BuscarTodos().ToList();
            var listaPaginada = Paginado<VestuarioModel>.From(paginacao, lista);

            if (listaPaginada.Resultado.Count == 0)
            {
                return NotFound();
            }

            return Ok(listaPaginada);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "Recupera a mercadoria identificado por seu {id}.",
            Tags = new[] { "Vestuario" },
            Produces = new[] { "application/json", "application/xml" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(VestuarioModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorId(
            [SwaggerParameter("Id da mercadoria.", Required = true)] int id)
        {
            var model = _mercadoriaDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo mercadoria na base.",
            Tags = new[] { "Vestuario" }
            )]
        [ProducesResponseType(statusCode: 201, Type = typeof(VestuarioModel))]
        [ProducesResponseType(statusCode: 400, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        public IActionResult Incluir([FromForm] VestuarioModel model)
        {
            if (ModelState.IsValid)
            {
                _mercadoriaDao.Incluir(model);
                var uri = Url.Action("RecuperarPorId", new { id = model.IdMercadoria });
                return Created(uri, model);
            }
            return BadRequest(ErroResponse.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o mercadoria na base.",
            Tags = new[] { "Vestuario" })]
        [ProducesResponseType(statusCode: 200)]
        [ProducesResponseType(400, Type = typeof(ErroResponse))]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Alterar([FromForm] VestuarioModel model)
        {
            if (ModelState.IsValid)
            {
                _mercadoriaDao.Alterar(model);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o mercadoria da base.",
            Tags = new[] { "Vestuario" }
        )]
        [ProducesResponseType(statusCode: 204)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Remover(int id)
        {
            var model = _mercadoriaDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            _mercadoriaDao.Excluir(model);
            return NoContent();
        }
    }
}