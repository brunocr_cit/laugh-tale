﻿using LaughTale.API.Model.Erros;
using LaughTale.API.Model.Ordenacao;
using LaughTale.API.Model.Paginacao;
using LaughTale.DAL.Funcionarios;
using LaughTale.Modelos;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;

namespace LaughTale.API.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class FuncionarioController : ControllerBase
    {
        private readonly FuncionarioRepository _funcionarioDao;

        public FuncionarioController(FuncionarioRepository funcionarioDao)
        {
            _funcionarioDao = funcionarioDao;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Recupera uma coleção paginada de funcionarios.",
            Tags = new[] { "Funcionario" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(Paginado<FuncionarioModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult ListaFuncionarios(
            [FromQuery] Ordem ordem,
            [FromQuery] Paginacao paginacao)
        {

            var lista = _funcionarioDao.BuscarTodos().ToList();
            var listaPaginada = Paginado<FuncionarioModel>.From(paginacao, lista);

            if (listaPaginada.Resultado.Count == 0)
            {
                return NotFound();
            }

            return Ok(listaPaginada);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "Recupera o funcionario identificado por seu {id}.",
            Tags = new[] { "Funcionario" },
            Produces = new[] { "application/json", "application/xml" }
        )]
        [ProducesResponseType(statusCode: 200, Type = typeof(FuncionarioModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorId(
            [SwaggerParameter("Id do funcionario.", Required = true)] int id)
        {
            var model = _funcionarioDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpGet("{nome}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(FuncionarioModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 404)]
        public IActionResult RecuperarPorNome(
            [SwaggerParameter("Nome do funcionario.", Required = true)] string nome)
        {
            var model = _funcionarioDao.BuscarPorNome(nome);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Registra novo funcionario na base.",
            Tags = new[] { "Funcionario" }
            )]
        [ProducesResponseType(statusCode: 201, Type = typeof(FuncionarioModel))]
        [ProducesResponseType(statusCode: 400, Type = typeof(ErroResponse))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponse))]
        public IActionResult Incluir([FromForm] FuncionarioModel model)
        {
            if (ModelState.IsValid)
            {
                _funcionarioDao.Incluir(model);
                var uri = Url.Action("RecuperarPorId", new { id = model.IdFuncionario });
                return Created(uri, model);
            }
            return BadRequest(ErroResponse.FromModelStateError(ModelState));
        }

        [HttpPut]
        [SwaggerOperation(
            Summary = "Modifica o funcionario na base.",
            Tags = new[] { "Funcionario" })]
        [ProducesResponseType(statusCode: 200)]
        [ProducesResponseType(400, Type = typeof(ErroResponse))]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Alterar([FromForm] FuncionarioModel model)
        {
            if (ModelState.IsValid)
            {
                _funcionarioDao.Alterar(model);
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(
            Summary = "Exclui o funcionario da base.",
            Tags = new[] { "Funcionario" }
        )]
        [ProducesResponseType(statusCode: 204)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(500, Type = typeof(ErroResponse))]
        public IActionResult Remover(int id)
        {
            var model = _funcionarioDao.BuscarPorId(id);
            if (model == null)
            {
                return NotFound();
            }
            _funcionarioDao.Excluir(model);
            return NoContent();
        }
    }
}
