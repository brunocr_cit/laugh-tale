﻿using LaughTale.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace LaughTale.API.Model.Ordenacao
{
    public static class OrdemExtensions
    {
        public static IQueryable AplicaOrdenacao(this IQueryable query, Ordem ordem)
        {
            if ((ordem != null)&&(!string.IsNullOrEmpty(ordem.OrdenarPor)))
            {
                query = query.OrderBy(ordem.OrdenarPor);
            }
            return query;
        }
    }

    public class Ordem 
    {
        public string OrdenarPor { get; set; }
    }
}
