using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LaughTale.API.Filtros;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace LaughTale.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
           //services.AddDbContext<LeituraContext>(options =>
           //{
           //    options.UseSqlServer(Configuration.GetConnectionString(""));
           //});
           //
           //services.AddTransient<IRepository<>, RepositorioBaseEF<>>();

           services.AddMvc(options =>
           { //filtro de exce��o
                   options.Filters.Add(typeof(ErroResponseExceptionFilter));
           }).AddXmlSerializerFormatters();

            services.AddSwaggerGen(options =>
            {

                options.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                //options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                //{
                //    Name = "Authorization",
                //    In = "header",
                //    Type = "apiKey",
                //    Description = "Autentica��o Bearer via JWT"
                //});
                //options.AddSecurityRequirement(
                //    new Dictionary<string, IEnumerable<string>> {
                //        { "Bearer", new string[] { } }
                //});

                options.EnableAnnotations();

                options.DescribeAllEnumsAsStrings();
                options.DescribeStringEnumsInCamelCase();

                options.DocumentFilter<TagDescriptionsDocumentFilter>();
                options.OperationFilter<AuthResponsesOperationFilter>();
                options.OperationFilter<AddInfoToParamVersionOperationFilter>();

               options.SwaggerDoc(
                    "v1.0",
                    new Info
                    {
                        Title = "Laugh Tale API",
                        Description = "API com servi�os relacionados a um comercio que vende artigos de presente.",
                        Version = "1.0"
                    }
                   //"v2.0",
                   //new Info
                   //{
                   //    Title = "Laugh Tale API",
                   //    Description = "API com servi�os relacionados a um comercio que vende artigos de presente.",
                   //    Version = "2.0"
                   //}
                );
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddApiVersioning();

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = "JwtBearer";
            //    options.DefaultChallengeScheme = "JwtBearer";
            //}).AddJwtBearer("JwtBearer", options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = true,
            //        ValidateAudience = true,
            //        ValidateLifetime = true,
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("laughtale-api-authentication-valid")), //Modificar no LoginController
            //        ClockSkew = TimeSpan.FromMinutes(5),
            //        ValidIssuer = "LaughTale.WebApp", //Modificar no LoginController
            //        ValidAudience = "Postman",
            //    };
            //});

            services.AddCors();
        }

        public void Configure(
            IApplicationBuilder app 
            //IHostingEnvironment env
            )
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            app.UseCors(builder => builder.WithOrigins("http://localhost"));

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Version 1.0");
                //c.SwaggerEndpoint("/swagger/v2.0/swagger.json", "Version 2.0");
            });

            app.UseAuthentication();

            //app.UseMvc();
        }


    }
}
