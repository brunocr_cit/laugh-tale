﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarCNPJServiceTests
    {
        [Theory]
        [InlineData("96.217.421/0001-48", true)]
        [InlineData("54.952.746/0001-94", false)]
        public void ValidarCNPJ(string cnpjASerTestado, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarCNPJService.ValidarCNPJ(cnpjASerTestado);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
