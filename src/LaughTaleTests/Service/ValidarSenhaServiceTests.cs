﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarSenhaServiceTests
    {
        [Theory]
        [InlineData("aBcDef10^^", true)]
        [InlineData("aBcdef^^", false)]
        [InlineData("ABCDEf10", false)]
        [InlineData("abcdef0^^", false)]
        [InlineData("ABCDEF10", false)]
        [InlineData("aBc10^^", false)]
        [InlineData("aBcDef10hdfnjxcushsfsdfgsfsadad", false)]
        public void ValidarEmail(string senhaASerTestada, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarSenhaService.ValidarSenha(senhaASerTestada);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
