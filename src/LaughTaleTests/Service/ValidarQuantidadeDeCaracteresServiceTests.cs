﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarQuantidadeDeCaracteresServiceTests
    {
        [Theory]
        [InlineData(50, 60, true)]
        [InlineData(70, 60, false)]
        public void ValidarQuantidadeDeCaracteres(int quantidadeExistente, int quantidadeMaxima, bool resultadoEsperado)
        {
            // Act 
            bool resultado = ValidarQuantidadeDeCaracteresService
                .ValidarQuantidadeDeCaracteres(quantidadeExistente, quantidadeMaxima);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
