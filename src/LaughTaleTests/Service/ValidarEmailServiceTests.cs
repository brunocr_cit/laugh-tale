﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarEmailServiceTests
    {
        [Theory]
        [InlineData("laughtale@hotmail.com", true)]
        [InlineData("email_invalido.com", false)]
        public void ValidarEmail(string emailASerTestado, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarEmailService.ValidarEmail(emailASerTestado);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
