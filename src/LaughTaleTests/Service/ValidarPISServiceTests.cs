﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarPISServiceTests
    {
        [Theory]
        [InlineData("342.97411.55-6", true)]
        [InlineData("541.86365.64-7", false)]
        public void ValidarPIS(string pisASerTestado, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarPISService.ValidarPIS(pisASerTestado);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
