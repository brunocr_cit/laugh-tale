﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarCEPServiceTests
    {
        [Theory]
        [InlineData("59092-548", true)]
        [InlineData("87566235", false)]
        public void ValidarCNPJ(string cnpjASerTestado, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarCEPService.ValidarCEP(cnpjASerTestado);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
