﻿using LaughTale.API.Service;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LaughTaleTests.Service
{
    public class ValidarCPFServiceTests
    {
        [Theory]
        [InlineData("253.706.690-15", true)]
        [InlineData("765.394.148-25", false)]
        public void ValidarCPF(string cpfASerTestado, bool resultadoEsperado)
        {
            //Act
            bool resultado = ValidarCPFService.ValidarCPF(cpfASerTestado);

            //Assert
            Assert.Equal(resultadoEsperado, resultado);
        }
    }
}
