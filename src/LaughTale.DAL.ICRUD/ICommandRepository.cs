﻿namespace LaughTale.DAL.CRUD
{
    public interface ICommandRepository<T>
    {
        void Incluir(T obj);
        void Alterar(T obj);
        void Excluir(T obj);
    }
}
