﻿using System.Collections.Generic;

namespace LaughTale.DAL.CRUD
{
    public interface IQueryRepository<T>
    {
        IEnumerable<T> BuscarTodos();
        T BuscarPorId(int id);
    }
}
